<?php 
if (!extension_loaded('gd')) {
    exit('GD拡張モジュールが読み込まれていません。');
}

define('TRUE_TYPE_DIR', '/usr/share/fonts/truetype/');

exec('find '.TRUE_TYPE_DIR.' -name "*.ttf"', $aryFontPath);

$image = ImageCreate(1000, count($aryFontPath)*20+10);
$white = ImageColorAllocate($image, 0xFF, 0xFF, 0xFF);
$black = ImageColorAllocate($image, 0x00, 0x00, 0x00);

$i = 1;
foreach($aryFontPath as $fontpath) {
    $fontpath = str_replace(TRUE_TYPE_DIR, '', $fontpath);
    $output   = dirname($fontpath) . DIRECTORY_SEPARATOR 
              . basename($fontpath, '.ttf') 
              . ': abc, ABC, あいう';
    ImageTTFText($image, 13, 0, 10, 20*$i++, $black, $fontpath, $output);
}

header('Content-type: image/png');
ImagePng($image);