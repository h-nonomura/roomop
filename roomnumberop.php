<?php

session_cache_limiter("public");
session_start();

require "roomconfig.php";

$prmarray = cnv_formstr($_POST);

if (isset($prmarray["act"])) {

$act = $prmarray["act"];

}

else {

$act = DEFSCR;
}

date_default_timezone_set('Asia/Tokyo');
$dt = date("Y-m-d H:i:s");

?>
<?php $conn = db_conn(); ?>
<html>

<head>
<meta http—equiv="content—type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="default.css" type="text/css" />
<title><?=ADMINAPPNAME?></title>

</head>

<body bgcolor="#e8ffe8">

<div align="center">

<?php
call_user_func("screen_".$act, $prmarray);
?>

</div>

</body>

</html>

<?php db_close($conn); ?>

<?php

function screen_src($array) {
$key = (isset($array["key"])) ? $array["key"] : "";

$p = (isset($array["p"])) ? intval($array["p"]) : 1;
$p = ($p < 1) ? 1 : $p;

?>

<?php disp_menu(); ?>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="0">
<tr>
<td><input type="text" name="key" value="<?=$key?>"></td>
<td><input type="submit" value="検索" name="sub1"></td>

</tr>
</table>
<input type="hidden" name="act" value="src">
</form>
<?php disp_listdata($key, $p); ?>
<?php
}

function screen_ent() {

?>

<?php disp_menu(); ?>
<h3>登録画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>部屋番号</td>
<td><input type="text" name="roomnumber_no" size="100"></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="登録確認" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="act" value="entconf">
</form>
<?php
}
function screen_entconf($array) {
if (!chk_data($array)) { return; }
extract($array);
?>

<?php disp_menu(); ?>
<h3>登録確認画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>部屋番号</td>
<td><?=$roomnumber_no?></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="登録実行" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="roomnumber_no" value="<?=$roomnumber_no?>">
<input type="hidden" name="act" value="dojob">
<input type="hidden" name="kbn" value="ent">
</form>
<?php
}

function screen_upd($array) {
if (!isset($array["roomnumber_id"])) { return; }

$row = get_data($array["roomnumber_id"]);
?>

<?php disp_menu(); ?>

<h3>更新画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>部屋番号ID</td>
<td><?=$row["roomnumber_id"]?></td>
</tr>
<tr>
<td>部屋番号</td>
<td><input type="text" name="roomnumber_no" value="<?=$row["roomnumber_no"]?>" size="100"></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="更新確認" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="act" value="updconf">
<input type="hidden" name="roomnumber_id" value="<?=$row["roomnumber_id"]?>">
</form>
<?php
}

function screen_updconf($row) {

if (!chk_data($row)) { return; }
?>

<?php disp_menu(); ?>
<h3>更新確認画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>部屋番号ID</td>
<td><?=$row["roomnumber_id"]?></td>
</tr>
<tr>
<tr>
<td>部屋番号</td>
<td><?=$row["roomnumber_no"]?></td>
</tr>
<td> </td>
<td><input type="submit" value="更新実行" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="roomnumber_id" value="<?=$row["roomnumber_id"]?>">
<input type="hidden" name="roomnumber_no" value="<?=$row["roomnumber_no"]?>">
<input type="hidden" name="act" value="dojob">
<input type="hidden" name="kbn" value="upd">
</form>
<?php
}

function screen_delconf($array) {
if (!isset($array["roomnumber_id"])) { return; }
$row = get_data($array["roomnumber_id"]);

?>

<?php disp_menu(); ?>
<h3>削除確認画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>部屋番号ID</td>
<td><?=$row["roomnumber_id"]?></td>
</tr>
<tr>
<tr>
<td>部屋番号</td>
<td><?=$row["roomnumber_no"]?></td>
</tr>
<td> </td>
<td><input type="submit" value="削除実行" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="roomnumber_id" value="<?=$row["roomnumber_id"]?>">
<input type="hidden" name="act" value="dojob">
<input type="hidden" name="kbn" value="del">
</form>
<?php
}

function screen_dojob($array) {
$res_mes = db_update($array);
?>

<p><?php disp_menu(); ?>
<h3>処理完了画面</h3>

<table border="0" bgcolor="pink">
<tr>
<td>処理完了、</td>
<td><?=$res_mes; ?></td>
</tr>
</table>
<?php
}

function chk_data($array) {

$strerr = "";

if ($array["roomnumber_no"] == "") {
echo "<p>部屋番号が入力されてないよ";
$strerr = "1";
}
if ($strerr == "1") {
return FALSE; 
}
else {

return TRUE;
}
}

function cnv_formstr($array) {

foreach($array as $k => $v){
if (get_magic_quotes_gpc()) {
$v = stripslashes($v);
}
$v = htmlspecialchars($v);
$array[$k] = $v;
}
return $array;
}

function cnv_sqlstr($string) {
$det_enc = mb_detect_encoding($string,"UTF-8");
if ($det_enc and $det_enc != ENCDB) {
$string = mb_convert_encoding($string, ENCDB, $det_enc);
}

$string = addslashes($string);
return $string;
}

function cnv_dispstr($string) {
$det_enc = mb_detect_encoding($string, "UTF-8");
if ($det_enc and $det_enc != ENCDISP) {
return mb_convert_encoding($string, ENCDISP, $det_enc);

}
else {
return $string;
}
}

function cnv_link($url, $title) {
$string = "<a href=\"$url\">".$title."</a>";
return $string;
}

function get_data($roomnumber_id) {
global $conn;

$sql = "SELECT * FROM roomnumber";
$sql .= " WHERE (roomnumber.roomnumber_id = '".cnv_sqlstr($roomnumber_id)."')";
$res = db_query($sql, $conn) or die("データ抽出エラー");
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
return $row;
}

function disp_listdata($key, $p) {

global $conn;

$st = ($p - 1) * intval(ADMINPAGESIZE);

$sql = "SELECT * FROM roomnumber";
if (strlen($key) > 0) {
$sql .= " WHERE (roomnumber_no LIKE '%".cnv_sqlstr($key)."%')";
}
$sql .= " ORDER BY roomnumber.roomnumber_id";
$res = db_query($sql, $conn) or die("データ抽出エラー");
if (mysqli_num_rows($res) <= 0) {
echo "<p>データは登録されていません";
return;
}
?>

<table border="1" bgcolor="white">
<tr>
<td> </td>
<td>部屋番号ID</td>
<td>部屋番号</td>
</tr>
<?php $i = 0 ?>
<?php while ($row = mysqli_fetch_array($res, MYSQL_ASSOC)) { ?>
<tr 
<?php if(($i % 2) == 1){ ?>
	style="background:whitesmoke"
<?php }else{ ?>
	style="background:lightgrey"
<?php } ?>>
<td>
<table>
<tr>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td><input type="submit" value="更新"></td>
<input type="hidden" name="act" value="upd">
<input type="hidden" name="roomnumber_id" value="<?=$row["roomnumber_id"]?>">
</form>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td width="50%"><input type="submit" value="削除"></td>

<input type="hidden" name="act" value="delconf">
<input type="hidden" name="roomnumber_id" value="<?=$row["roomnumber_id"]?>">
</form>
</tr>
</table>
</td>
<td><?=cnv_dispstr($row["roomnumber_id"])?></td>
<td><?=cnv_dispstr($row["roomnumber_no"])?></td>
</tr>
<?php $i++; } ?>
</table>

<?php disp_pagenav($key, $p); ?>
<?php
}

function disp_menu() {
?>
<table border="1">
<tr>
<th colspan="8"><big><b><?=ADMINAPPNAME?></b></big></th>
</tr>
<tr style="background:lightyellow">
<form method="POST" action="roomop.php">
<td><input type="submit" value="部屋の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="roomop.php">
<td><input type="submit" value="部屋の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
<tr style="background:#e0f0ff">
<form method="POST" action="buildingop.php">
<td><input type="submit" value="棟の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="buildingop.php">
<td><input type="submit" value="棟の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
<tr style="background:#e8ffe8">
<form method="POST" action="roomnumberop.php">
<td><input type="submit" value="部屋番号の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="roomnumberop.php">
<td><input type="submit" value="部屋番号の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
<tr style="background:#f5f5f5">
<form method="POST" action="beacon_identifierop.php">
<td><input type="submit" value="識別子の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="beacon_identifierop.php">
<td><input type="submit" value="識別子の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
</table>
<?php
}

function disp_pagenav($key, $p = 1) {
global $conn;
$prev = $p - 1;
$prev = ($prev < 1) ? 1 : $prev;
$next = $p + 1;

$sql = "SELECT COUNT(*) as cnt FROM roomnumber";
if (isset($key)) {
if (strlen($key) > 0) {
$sql .= " WHERE (roomnumber_no LIKE '%".cnv_sqlstr($key)."%')";
}
}
$res = db_query($sql, $conn) or die("データ抽出エラー");
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
$recordcount = $row["cnt"];
?>

<table>
<tr>
<?php if ($p > 1) { ?>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td><input type="submit" value="<< 前"></td>
<input type="hidden" name="act" value="src">
<input type="hidden" name="p" value="<?=$prev?>">
<input type="hidden" name="key" value="<?=$key?>">
</form>
<?php } ?>
<?php if ($recordcount > ($next - 1) * intval(ADMINPAGESIZE)) { ?>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td width="50%"><input type="submit" value="次 >>"></td>
<input type="hidden" name="act" value="src">
<input type="hidden" name="p" value="<?=$next?>">
<input type="hidden" name="key" value="<?=$key?>">
</form>
<?php } ?>
</tr>
</table>
<?php
}
function db_conn() {
//mysqliに変更
$conn = mysqli_connect(DBSV, DBUSER, DBPASS, DBNAME) or die("接続エラー");
//mysql_select_db(DBNAME, $conn);
return $conn;
}

function db_query($sql, $conn) {
$res = mysqli_query($conn, $sql);
return $res;
}

function db_update($array) {
global $conn;
global $dt;
if (!isset($array["kbn"])) { return "パラメータエラー"; }
if ($array["kbn"] != "del") {
if (!chk_data($array)) { return "パラメータエラー"; }
}
if ($array["kbn"] != "ent") {
if (!isset($array["roomnumber_id"])) { return "パラメータエラー"; }
}

extract($array);

//登録の処理
if ($kbn == "ent") {
$sql = "INSERT INTO roomnumber (";
$sql .= " roomnumber_id, ";
$sql .= " roomnumber_no ";
$sql .= ") VALUES (";
$sql .= " NULL, ";
$sql .= "'" . cnv_sqlstr($roomnumber_no) . "'";
$sql .= ")";

$res = db_query($sql, $conn);

if ($res) {
$str = "部屋番号に" . cnv_sqlstr($roomnumber_no) . "を追加したよ";
}
else {
$str = "";
}

return $str;
}

//更新の処理
if ($kbn == "upd") {
$sql = "UPDATE roomnumber SET ";
$sql .= " roomnumber_no = '" . cnv_sqlstr($roomnumber_no) . "'";
$sql .= " WHERE roomnumber_id = " . cnv_sqlstr($roomnumber_id);

$res = db_query($sql, $conn);

if ($res) {
$str = "更新完了したよ";
}
else {
$str = "更新失敗しちゃった…";
}
return $str;
}

//削除の処理
if ($kbn == "del") {
$sql = "DELETE FROM roomnumber ";
$sql .= "WHERE roomnumber.roomnumber_id = '" . cnv_sqlstr($roomnumber_id) . "'";
$res = db_query($sql, $conn);
if ($res) {
return "消しておいたよ。";
}
else {
return "消すの失敗しちゃった…";
}
}

}

function db_close($conn) {
mysqli_close($conn);
}
?>