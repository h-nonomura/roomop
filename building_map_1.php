<!-- Created with Inkscape (http://www.inkscape.org/) -->
<?php 
session_cache_limiter("public");
session_start();

require "roomconfig.php";

date_default_timezone_set('Asia/Tokyo');
$dt = date("Y-m-d H:i:s");

$building_name = "4号館";
$strong_roomnumber_no = $_POST["roomnumber_no"];
$font_size = 15;
$icon_size = 40;
$room_no_array = array("101","102","103","104","105","106","107","111","112","113","114",
					   "201","202","203","204","205","206","207","208","209","210","211","212","213","214",
					   "301","302","303","304","305","306","307","308","309","311","312","313","314","315","316",
					   "401","402","403","404");

?>
<?php $conn = db_conn(); ?>

<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?=$building_name?></title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="stylesheet" href="building_map_1.css">
</head>
<body>
<div id="wrapper">
  <input type="checkbox" id="navTgl">
 
  <label for="navTgl" class="open">≡</label>
  <label for="navTgl" class="close"></label>
  <nav class="menu">
    <h2>部屋一覧</h2>
    <ul>
	<?php foreach($room_no_array as $room_no){ ?>
      <li>
		  <table>
			<tr>
				<td>
					<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
					<input type="hidden" name="roomnumber_no" value="<?=$room_no?>">
					<input type="submit" id="menuSwitch<?=$room_no?>" class="none">
					<label for="menuSwitch<?=$room_no?>">
						<?=$room_no?>号室<br>
						<?php get_data($building_name, $room_no) ?><br>
						<nobr>ビーコン:<?php echo get_beacon_identifier(get_id($building_name, $room_no)); ?></nobr>
					</label>
					</form>
				</td>
				<td>
					<table>
						<tr>
							<form method="POST" action="roomop.php">
							<td><input type="submit" value="更新" class="menuButton"></td>
							<input type="hidden" name="act" value="upd">
							<input type="hidden" name="room_id" value="<?php echo get_id($building_name, $room_no); ?>">
							</form>
						</tr>
					</table>
				</td>
			</tr>
		  </table>
	  </li>
	<?php } ?>
    </ul>
  </nav>
 
  <div class="contents">
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   id="svg4835"
   version="1.1"
   inkscape:version="0.91 r13725"
   width="1474.8"
   height="1771.8"
   viewBox="0 0 1474.8 1771.8"
   sodipodi:docname="building_map_1.svg">
  <metadata
     id="metadata4841">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <defs
     id="defs4839" />
  <sodipodi:namedview
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1"
     objecttolerance="10"
     gridtolerance="10"
     guidetolerance="10"
     inkscape:pageopacity="0"
     inkscape:pageshadow="2"
     inkscape:window-width="1017"
     inkscape:window-height="664"
     id="namedview4837"
     showgrid="false"
     inkscape:zoom="0.34"
     inkscape:cx="754.03338"
     inkscape:cy="1096.491"
     inkscape:window-x="71"
     inkscape:window-y="10"
     inkscape:window-maximized="0"
     inkscape:current-layer="layer2" />
  <g
     inkscape:groupmode="layer"
     id="layer1"
     inkscape:label="Layer 1"
     style="display:inline"
     sodipodi:insensitive="true">
    <image
       sodipodi:absref="/Applications/MAMP/htdocs/PHP/roomop/building_map_1.png"
       xlink:href="building_map_1.png"
       y="0"
       x="0"
       id="image4843"
       preserveAspectRatio="none"
       height="1771.8"
       width="1474.8" />
  </g>
  <g
     inkscape:groupmode="layer"
     id="layer2"
     inkscape:label="Layer 2">
	<!-- ビーコン操作画面へ遷移するためのスクリプト -->
	<script><![CDATA[
	function beacon_remove(room_id){
		window.open('', 'remove', 'width=800,height=600,scrollbars=yes');
		var form = document.createElement('form');
		document.body.appendChild(form);
		var input = document.createElement('input');
		input.setAttribute('type', 'hidden');
		input.setAttribute('name', 'room_id');
		input.setAttribute('value', room_id);
		form.appendChild(input);
		form.setAttribute('action', 'map_beacon_remove.php');
		form.setAttribute('target', 'remove');
		form.setAttribute('method', 'post');
		form.submit();
	}
	
	function beacon_append(room_id){
		window.open('', 'append', 'width=800,height=600,scrollbars=yes');
		var form = document.createElement('form');
		document.body.appendChild(form);
		var input = document.createElement('input');
		input.setAttribute('type', 'hidden');
		input.setAttribute('name', 'room_id');
		input.setAttribute('value', room_id);
		form.appendChild(input);
		form.setAttribute('action', 'map_beacon_append.php');
		form.setAttribute('target', 'append');
		form.setAttribute('method', 'post');
		form.submit();
	}
	
	function scroll(roomnumber_no, point){
		if(roomnumber_no == <?=$strong_roomnumber_no?>){
			window.scrollTo(0, point);
		}
	}
	]]></script>
    <path
	   <?php 
       //部屋番号
       $roomnumber_no = "401";
       //左上の座標
       $ulx = 84.52381;
       $uly = 303.94291;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 270.23809;
       //右下の座標
       $drx = $dlx + 371.42857;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 270.23809;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4950"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
	</form>
    <path
	   <?php 
       //部屋番号
       $roomnumber_no = "402";
       //左上の座標
       $ulx = 455.95238;
       $uly = 458.70481;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.47619;
       //右下の座標
       $drx = $dlx + 279.76191;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.47619;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4952"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
	   <?php 
       //部屋番号
       $roomnumber_no = "403";
       //左上の座標
       $ulx = 735.71429;
       $uly = 458.70481;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 114.28572;
       //右下の座標
       $drx = $dlx + 230.95238;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 114.28572;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4954"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
	   <?php 
       //部屋番号
       $roomnumber_no = "404";
       //左上の座標
       $ulx = 965;
       $uly = 302;
       //左下の座標
       $dlx = $ulx + 0;
       $dly = $uly + 270;
       //右下の座標
       $drx = $dlx + 376;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 269;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4956"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
	   <?php 
       //部屋番号
       $roomnumber_no = "311";
       //左上の座標
       $ulx = 84.0;
       $uly = 680.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 114.0;
       //右下の座標
       $drx = $dlx + 92.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 114.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4958"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "312";
       //左上の座標
       $ulx = 177.0;
       $uly = 680.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 114.0;
       //右下の座標
       $drx = $dlx + 186.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 113.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4960"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "313";
       //左上の座標
       $ulx = 363.0;
       $uly = 680.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 114.0;
       //右下の座標
       $drx = $dlx + 94.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4962"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "314";
       //左上の座標
       $ulx = 455.0;
       $uly = 678.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 116.0;
       //右下の座標
       $drx = $dlx + 186.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 116.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4964"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "315";
       //左上の座標
       $ulx = 966.0;
       $uly = 678.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 116.0;
       //右下の座標
       $drx = $dlx + 186.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 116.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4966"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "316";
       //左上の座標
       $ulx = 1152.0;
       $uly = 678.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 116.0;
       //右下の座標
       $drx = $dlx + 189.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4968"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "301";
       //左上の座標
       $ulx = 84.0;
       $uly = 833.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 186.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4970"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "302";
       //左上の座標
       $ulx = 270.0;
       $uly = 833.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 114.0;
       //右下の座標
       $drx = $dlx + 95.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4972"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "303";
       //左上の座標
       $ulx = 364.0;
       $uly = 833.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 92.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 117.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4974"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "304";
       //左上の座標
       $ulx = 455.0;
       $uly = 834.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 114.0;
       //右下の座標
       $drx = $dlx + 186.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 116.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4976"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "305";
       //左上の座標
       $ulx = 642.0;
       $uly = 833.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 114.0;
       //右下の座標
       $drx = $dlx + 94.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4978"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "306";
       //左上の座標
       $ulx = 735.0;
       $uly = 833.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 232.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 113.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4980"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "307";
       //左上の座標
       $ulx = 966.0;
       $uly = 832.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 92.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 116.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4982"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "308";
       //左上の座標
       $ulx = 1059.0;
       $uly = 834.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 114.0;
       //右下の座標
       $drx = $dlx + 94.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4984"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "309";
       //左上の座標
       $ulx = 1152.0;
       $uly = 833.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 114.0;
       //右下の座標
       $drx = $dlx + 189.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 114.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4986"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "211";
       //左上の座標
       $ulx = 84.0;
       $uly = 1056.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 116.0;
       //右下の座標
       $drx = $dlx + 186.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4988"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "212";
       //左上の座標
       $ulx = 271.0;
       $uly = 1056.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 186.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4990"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "213";
       //左上の座標
       $ulx = 455.0;
       $uly = 1056.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 186.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4992"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "214";
       //左上の座標
       $ulx = 967.0;
       $uly = 1057.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 114.0;
       //右下の座標
       $drx = $dlx + 185.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4994"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "210";
       //左上の座標
       $ulx = 1154.0;
       $uly = 1056.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 191.0;
       //右下の座標
       $drx = $dlx + 185.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 191.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4996"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "201";
       //左上の座標
       $ulx = 84.0;
       $uly = 1209.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 92.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 114.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4998"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "202";
       //左上の座標
       $ulx = 177.0;
       $uly = 1211.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 186.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5000"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "203";
       //左上の座標
       $ulx = 364.0;
       $uly = 1211.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 92.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5002"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "204";
       //左上の座標
       $ulx = 456.0;
       $uly = 1210.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 118.0;
       //右下の座標
       $drx = $dlx + 187.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5004"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "205";
       //左上の座標
       $ulx = 642.0;
       $uly = 1212.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 113.0;
       //右下の座標
       $drx = $dlx + 92.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5006"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "206";
       //左上の座標
       $ulx = 735.0;
       $uly = 1209.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 116.0;
       //右下の座標
       $drx = $dlx + 232.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 116.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5008"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "207";
       //左上の座標
       $ulx = 967.0;
       $uly = 1211.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 91.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 116.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5010"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "208";
       //左上の座標
       $ulx = 1059.0;
       $uly = 1212.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 113.0;
       //右下の座標
       $drx = $dlx + 94.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 111.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5012"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "209";
       //左上の座標
       $ulx = 1154.0;
       $uly = 1250.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 75.0;
       //右下の座標
       $drx = $dlx + 185.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 76.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5014"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "111";
       //左上の座標
       $ulx = 84.0;
       $uly = 1432.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 116.0;
       //右下の座標
       $drx = $dlx + 185.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5016"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "112";
       //左上の座標
       $ulx = 271.0;
       $uly = 1434.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 114.0;
       //右下の座標
       $drx = $dlx + 185.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 114.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5018"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "113";
       //左上の座標
       $ulx = 457.0;
       $uly = 1434.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 116.0;
       //右下の座標
       $drx = $dlx + 92.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 116.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5020"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "114";
       //左上の座標
       $ulx = 548.0;
       $uly = 1434.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 92.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 116.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5022"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "101";
       //左上の座標
       $ulx = 83.0;
       $uly = 1588.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 116.0;
       //右下の座標
       $drx = $dlx + 92.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 117.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5024"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "102";
       //左上の座標
       $ulx = 177.0;
       $uly = 1587.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 116.0;
       //右下の座標
       $drx = $dlx + 187.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5026"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "103";
       //左上の座標
       $ulx = 364.0;
       $uly = 1588.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 113.0;
       //右下の座標
       $drx = $dlx + 92.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 113.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5028"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "104";
       //左上の座標
       $ulx = 457.0;
       $uly = 1588.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 185.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5030"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "105";
       //左上の座標
       $ulx = 642.0;
       $uly = 1590.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 112.0;
       //右下の座標
       $drx = $dlx + 94.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5032"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "106";
       //左上の座標
       $ulx = 735.0;
       $uly = 1588.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 177.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 115.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5034"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       
	   <?php 
       //部屋番号
       $roomnumber_no = "107";
       //左上の座標
       $ulx = 966.0;
       $uly = 1588.0;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 115.0;
       //右下の座標
       $drx = $dlx + 94.0;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 113.0;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
	   //横の長さ
	   $width = $urx - $ulx;
	   //アイコンの場所
	   $icon_point = $width;
       ?>
	   style="fill:none;fill-rule:evenodd;<?php stroke($roomnumber_no); ?>stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path5036"
       inkscape:connector-curvature="0"
	   onload="scroll(<?=$roomnumber_no?>, <?=$uly?>)">
	   <title><?=$roomnumber_no?></title>
	</path>
	<?php for($i = 0;$i < substr_count(get_beacon_identifier(get_id($building_name, $roomnumber_no)), " ");$i++){
	 ?>
		<image xlink:href="beacon_icon.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_remove(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<?php 
		$icon_point = $icon_point - $icon_size;
	} ?>
	<image xlink:href="beacon_icon_none.png" preserveAspectRatio="xMaxYMin" x="<?=$ulx?>" y="<?=$uly?>" width="<?=$icon_point?>" height="<?=$icon_size?>" onclick="beacon_append(<?=get_id($building_name, $roomnumber_no)?>);"/>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
  </g>
</svg>
  </div>
</div>
</body>
</html>
<?php 
function get_data($building_name, $roomnumber_no) {
	global $conn;
	global $strong_roomnumber_no;
	
	//棟IDを取得
	$sql = "SELECT * FROM building ";
	$sql .= " WHERE (building.building_name = '".cnv_sqlstr($building_name)."')";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$building_id = $row["building_id"];
	
	//部屋番号IDを取得
	$sql = "SELECT * FROM roomnumber ";
	$sql .= " WHERE (roomnumber.roomnumber_no =".cnv_sqlstr($roomnumber_no).")";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$roomnumber_id = $row["roomnumber_id"];
	
	//登録されたroomdataのroom_nameを調べる
	$sql = "SELECT * FROM roomdata ";
	$sql .= " WHERE (roomdata.roomnumber_id =".cnv_sqlstr($roomnumber_id).")";
	$sql .= " AND (roomdata.building_id =".cnv_sqlstr($building_id).")";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$room_name = $row["room_name"];
	
	if($roomnumber_no === $strong_roomnumber_no){
		$room_name = "<tspan font-weight=\"bolder\" fill=\"red\">" . $room_name . "</tspan>";
	}
	
	echo $room_name;
}

function db_conn() {
//mysqliに変更
$conn = mysqli_connect(DBSV, DBUSER, DBPASS, DBNAME) or die("接続エラー");
return $conn;
}

function cnv_sqlstr($string) {
	$det_enc = mb_detect_encoding($string,"UTF-8");
	if ($det_enc and $det_enc != ENCDB) {
		$string = mb_convert_encoding($string, ENCDB, $det_enc);
	}
	$string = addslashes($string);
	return $string;
}

function db_query($sql, $conn) {
$res = mysqli_query($conn, $sql);
return $res;
}

function get_id($building_name, $roomnumber_no) {
	global $conn;
	
	//棟IDを取得
	$sql = "SELECT * FROM building ";
	$sql .= " WHERE (building.building_name = '".cnv_sqlstr($building_name)."')";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$building_id = $row["building_id"];
	
	//部屋番号IDを取得
	$sql = "SELECT * FROM roomnumber ";
	$sql .= " WHERE (roomnumber.roomnumber_no =".cnv_sqlstr($roomnumber_no).")";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$roomnumber_id = $row["roomnumber_id"];
	
	//登録されたroomdataのroom_nameを調べる
	$sql = "SELECT * FROM roomdata ";
	$sql .= " WHERE (roomdata.roomnumber_id =".cnv_sqlstr($roomnumber_id).")";
	$sql .= " AND (roomdata.building_id =".cnv_sqlstr($building_id).")";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$room_id = $row["room_id"];
	
	return $room_id;
}

function get_beacon_identifier($room_id) {
	global $conn;
	
	$sql = "SELECT * FROM beacon_identifier";
	$sql .= " WHERE (beacon_identifier.room_id = '".cnv_sqlstr($room_id)."')";
	$res = db_query($sql, $conn);
	$beacon_identifier = "";
	while ($row = mysqli_fetch_array($res, MYSQL_ASSOC)) {
		$beacon_identifier .= $row["beacon_identifier"] . " ";
	}
	
	return $beacon_identifier;
}

function stroke($roomnumber_no){
	global $strong_roomnumber_no;
	
	if($roomnumber_no === $strong_roomnumber_no){
		echo "stroke:red;stroke-width:3px;";
	}else{
		echo "stroke:black#000000;stroke-width:1px;";
	}
}
 ?>