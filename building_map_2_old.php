<?php 
session_cache_limiter("public");
session_start();

require "roomconfig.php";

date_default_timezone_set('Asia/Tokyo');
$dt = date("Y-m-d H:i:s");

$building_name = "4号館別館";
$strong_roomnumber_no = $_POST["roomnumber_no"];

//対象画像のリソースを取得
$image = imagecreatefromjpeg("building_map_2.jpg");
 
//画像リソースと、文字の色を第2~4引数にRGBで指定して色IDを取得します
//サンプルでは16進数で指定していますが、10進数でも問題ありません
$color = imagecolorallocate($image, 0x00, 0x00, 0x00);
 
//対象の画像リソース、フォントサイズ、文字列角度、文字のX座標、文字のY座標、色ID、フォントファイル、文字列　の順に指定します
imagettftext($image, 20, 0, 35, 50, $color, "MS Gothic.ttf", "やったぜ。");
 
//画像をファイルに書き出し
//リソース、ファイル名、品質を引数に与えます。品質は100が最高です
imagejpeg($image, "to.jpg", 100);

//リソースを解放する
imagecolordeallocate($image,$color);
imagedestroy($image);

?>
<?php $conn = db_conn(); ?>

<html>

<head>
<meta http—equiv="content—type" content="text/html; charset=utf-8">
<title>マップ</title>
</head>
<body>
<img src="to.jpg">
</body>
</html>

<?php 
function get_data($building_name, $roomnumber_no) {
	global $conn;
	global $strong_roomnumber_no;
	
	//棟IDを取得
	$sql = "SELECT * FROM building ";
	$sql .= " WHERE (building.building_name = '".cnv_sqlstr($building_name)."')";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$building_id = $row["building_id"];
	
	//部屋番号IDを取得
	$sql = "SELECT * FROM roomnumber ";
	$sql .= " WHERE (roomnumber.roomnumber_no =".cnv_sqlstr($roomnumber_no).")";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$roomnumber_id = $row["roomnumber_id"];
	
	//登録されたroomdataのroom_nameを調べる
	$sql = "SELECT * FROM roomdata ";
	$sql .= " WHERE (roomdata.roomnumber_id =".cnv_sqlstr($roomnumber_id).")";
	$sql .= " AND (roomdata.building_id =".cnv_sqlstr($building_id).")";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$room_name = $row["room_name"];
	
	if($roomnumber_no === $strong_roomnumber_no){
		$room_name = "<strong style=\"color:#dd0806;\">" . $room_name . "</strong>";
	}
	
	echo $room_name;
}

function db_conn() {
//mysqliに変更
$conn = mysqli_connect(DBSV, DBUSER, DBPASS, DBNAME) or die("接続エラー");
return $conn;
}

function cnv_sqlstr($string) {
	$det_enc = mb_detect_encoding($string,"UTF-8");
	if ($det_enc and $det_enc != ENCDB) {
		$string = mb_convert_encoding($string, ENCDB, $det_enc);
	}
	$string = addslashes($string);
	return $string;
}

function db_query($sql, $conn) {
$res = mysqli_query($conn, $sql);
return $res;
}
 ?>
