<?php

session_cache_limiter("public");
session_start();

require "roomconfig.php";

$prmarray = cnv_formstr($_POST);

if (isset($prmarray["act"])) {

$act = $prmarray["act"];

}

else {

$act = DEFSCR;
}

date_default_timezone_set('Asia/Tokyo');
$dt = date("Y-m-d H:i:s");

?>
<?php $conn = db_conn(); ?>
<html>

<head>
<meta http—equiv="content—type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="default.css" type="text/css" />
<title><?=ADMINAPPNAME?></title>

</head>

<body bgcolor="#f5f5f5">

<div align="center">

<?php
call_user_func("screen_".$act, $prmarray);
?>

</div>

</body>

</html>

<?php db_close($conn); ?>

<?php

function screen_src($array) {
$key = (isset($array["key"])) ? $array["key"] : "";

$p = (isset($array["p"])) ? intval($array["p"]) : 1;
$p = ($p < 1) ? 1 : $p;

?>

<?php disp_menu(); ?>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="0">
<tr>
<td><input type="text" name="key" value="<?=$key?>"></td>
<td><input type="submit" value="検索" name="sub1"></td>

</tr>
</table>
<input type="hidden" name="act" value="src">
</form>
<?php disp_listdata($key, $p); ?>
<?php
}

function screen_ent() {

?>

<?php disp_menu(); ?>
<h3>登録画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>ビーコン識別子</td>
<td><input type="text" name="beacon_identifier" size="100"></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="登録確認" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="act" value="entconf">
</form>
<?php
}
function screen_entconf($array) {
if (!chk_data($array)) { return; }
extract($array);
?>

<?php disp_menu(); ?>
<h3>登録確認画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>ビーコン識別子</td>
<td><?=$beacon_identifier?></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="登録実行" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="beacon_identifier" value="<?=$beacon_identifier?>">
<input type="hidden" name="act" value="dojob">
<input type="hidden" name="kbn" value="ent">
</form>
<?php
}

function screen_upd($array) {
if (!isset($array["beacon_identifier"])) { return; }

$row = get_data($array["beacon_identifier"]);
$beacon_identifier_old = $array["beacon_identifier"];
?>

<?php disp_menu(); ?>

<h3>更新画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>識別子</td>
<td><input type="text" name="beacon_identifier" value="<?=$row["beacon_identifier"]?>" size="100"></td>
</tr>
<tr>
<td>部屋名</td>
<td><?=$row["room_name"]?></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="更新確認" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="act" value="updconf">
<input type="hidden" name="room_name" value="<?=$row["room_name"]?>">
<input type="hidden" name="beacon_identifier_old" value="<?=$beacon_identifier_old?>">
</form>
<?php
}

function screen_updconf($row) {

if (!chk_data($row)) { return; }
?>

<?php disp_menu(); ?>
<h3>更新確認画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>識別子</td>
<td><?=$row["beacon_identifier"]?></td>
</tr>
<tr>
<tr>
<td>部屋名</td>
<td><?=$row["room_name"]?></td>
</tr>
<td> </td>
<td><input type="submit" value="更新実行" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="beacon_identifier_old" value="<?=$row["beacon_identifier_old"]?>">
<input type="hidden" name="beacon_identifier" value="<?=$row["beacon_identifier"]?>">
<input type="hidden" name="room_name" value="<?=$row["room_name"]?>">
<input type="hidden" name="act" value="dojob">
<input type="hidden" name="kbn" value="upd">
</form>
<?php
}

function screen_delconf($array) {
if (!isset($array["beacon_identifier"])) { return; }
$row = get_data($array["beacon_identifier"]);

?>

<?php disp_menu(); ?>
<h3>削除確認画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>識別子</td>
<td><?=$row["beacon_identifier"]?></td>
</tr>
<tr>
<tr>
<td>部屋名</td>
<td><?=$row["room_name"]?></td>
</tr>
<td> </td>
<td><input type="submit" value="削除実行" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="beacon_identifier" value="<?=$row["beacon_identifier"]?>">
<input type="hidden" name="act" value="dojob">
<input type="hidden" name="kbn" value="del">
</form>
<?php
}

function screen_dojob($array) {
$res_mes = db_update($array);
?>

<p><?php disp_menu(); ?>
<h3>処理完了画面</h3>

<table border="0" bgcolor="pink">
<tr>
<td>処理完了、</td>
<td><?=$res_mes; ?></td>
</tr>
</table>
<?php
}

function chk_data($array) {

$strerr = "";

if ($array["beacon_identifier"] == "") {
echo "<p>ビーコン識別子が入力されてないよ";
$strerr = "1";
}
if ($strerr == "1") {
return FALSE; 
}
else {

return TRUE;
}
}

function cnv_formstr($array) {

foreach($array as $k => $v){
if (get_magic_quotes_gpc()) {
$v = stripslashes($v);
}
$v = htmlspecialchars($v);
$array[$k] = $v;
}
return $array;
}

function cnv_sqlstr($string) {
$det_enc = mb_detect_encoding($string,"UTF-8");
if ($det_enc and $det_enc != ENCDB) {
$string = mb_convert_encoding($string, ENCDB, $det_enc);
}

$string = addslashes($string);
return $string;
}

function cnv_dispstr($string) {
$det_enc = mb_detect_encoding($string, "UTF-8");
if ($det_enc and $det_enc != ENCDISP) {
return mb_convert_encoding($string, ENCDISP, $det_enc);

}
else {
return $string;
}
}

function cnv_link($url, $title) {
$string = "<a href=\"$url\">".$title."</a>";
return $string;
}

function get_data($beacon_identifier) {
global $conn;

$sql = "SELECT beacon_identifier, room_name FROM beacon_identifier";
$sql .= " left outer join roomdata on (beacon_identifier.room_id = roomdata.room_id)";
$sql .= " WHERE (beacon_identifier.beacon_identifier = '".cnv_sqlstr($beacon_identifier)."')";
$res = db_query($sql, $conn) or die("データ抽出エラー");
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
return $row;
}

function disp_listdata($key, $p) {

global $conn;

$st = ($p - 1) * intval(ADMINPAGESIZE);

$sql = "SELECT beacon_identifier, room_name FROM beacon_identifier";
$sql .= " left outer join roomdata on (beacon_identifier.room_id = roomdata.room_id)";
if (strlen($key) > 0) {
$sql .= " WHERE (beacon_identifier LIKE '%".cnv_sqlstr($key)."%')";
$sql .= " OR (room_name LIKE '%".cnv_sqlstr($key)."%')";
}
$sql .= " ORDER BY beacon_identifier.beacon_identifier";
$res = db_query($sql, $conn) or die("データ抽出エラー");
if (mysqli_num_rows($res) <= 0) {
echo "<p>データは登録されていません";
return;
}
?>

<table border="1" bgcolor="white">
<tr>
<td> </td>
<td>ビーコン識別子</td>
<td>部屋名</td>
</tr>
<?php $i = 0 ?>
<?php while ($row = mysqli_fetch_array($res, MYSQL_ASSOC)) { ?>
<tr 
<?php if(($i % 2) == 1){ ?>
	style="background:whitesmoke"
<?php }else{ ?>
	style="background:lightgrey"
<?php } ?>>
<td>
<table>
<tr>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td><input type="submit" value="更新"></td>
<input type="hidden" name="act" value="upd">
<input type="hidden" name="beacon_identifier" value="<?=$row["beacon_identifier"]?>">
</form>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td width="50%"><input type="submit" value="削除"></td>
<input type="hidden" name="act" value="delconf">
<input type="hidden" name="beacon_identifier" value="<?=$row["beacon_identifier"]?>">
</form>
</tr>
</table>
</td>
<td><?=cnv_dispstr($row["beacon_identifier"])?></td>
<td><?=cnv_dispstr($row["room_name"])?></td>
</tr>
<?php $i++; } ?>
</table>

<?php disp_pagenav($key, $p); ?>
<?php
}

function disp_menu() {
?>
<table border="1">
<tr>
<th colspan="8"><big><b><?=ADMINAPPNAME?></b></big></th>
</tr>
<tr style="background:lightyellow">
<form method="POST" action="roomop.php">
<td><input type="submit" value="部屋の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="roomop.php">
<td><input type="submit" value="部屋の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
<tr style="background:#e0f0ff">
<form method="POST" action="buildingop.php">
<td><input type="submit" value="棟の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="buildingop.php">
<td><input type="submit" value="棟の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
<tr style="background:#e8ffe8">
<form method="POST" action="roomnumberop.php">
<td><input type="submit" value="部屋番号の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="roomnumberop.php">
<td><input type="submit" value="部屋番号の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
<tr style="background:#f5f5f5">
<form method="POST" action="beacon_identifierop.php">
<td><input type="submit" value="識別子の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="beacon_identifierop.php">
<td><input type="submit" value="識別子の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
</table>
<?php
}

function disp_pagenav($key, $p = 1) {
global $conn;
$prev = $p - 1;
$prev = ($prev < 1) ? 1 : $prev;
$next = $p + 1;

$sql = "SELECT beacon_identifier, room_name, COUNT(beacon_identifier.room_id) as cnt FROM beacon_identifier";
$sql .= " left outer join roomdata on (beacon_identifier.room_id = roomdata.room_id)";
if (isset($key)) {
if (strlen($key) > 0) {
$sql .= " WHERE (beacon_identifier LIKE '%".cnv_sqlstr($key)."%')";
$sql .= " OR (room_name LIKE '%".cnv_sqlstr($key)."%')";
}
}
$res = db_query($sql, $conn) or die("データ抽出エラー");
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
$recordcount = $row["cnt"];
?>

<table>
<tr>
<?php if ($p > 1) { ?>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td><input type="submit" value="<< 前"></td>
<input type="hidden" name="act" value="src">
<input type="hidden" name="p" value="<?=$prev?>">
<input type="hidden" name="key" value="<?=$key?>">
</form>
<?php } ?>
<?php if ($recordcount > ($next - 1) * intval(ADMINPAGESIZE)) { ?>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td width="50%"><input type="submit" value="次 >>"></td>
<input type="hidden" name="act" value="src">
<input type="hidden" name="p" value="<?=$next?>">
<input type="hidden" name="key" value="<?=$key?>">
</form>
<?php } ?>
</tr>
</table>
<?php
}
function db_conn() {
//mysqliに変更
$conn = mysqli_connect(DBSV, DBUSER, DBPASS, DBNAME) or die("接続エラー");
//mysql_select_db(DBNAME, $conn);
return $conn;
}

function db_query($sql, $conn) {
$res = mysqli_query($conn, $sql);
return $res;
}

function db_update($array) {
global $conn;
global $dt;
if (!isset($array["kbn"])) { return "パラメータエラー"; }
if ($array["kbn"] != "del") {
if (!chk_data($array)) { return "パラメータエラー"; }
}
if ($array["kbn"] != "ent") {
if (!isset($array["beacon_identifier"])) { return "パラメータエラー"; }
}

extract($array);

//登録の処理
if ($kbn == "ent") {
$sql = "INSERT INTO beacon_identifier (";
$sql .= " beacon_identifier, ";
$sql .= " room_id ";
$sql .= ") VALUES (";
$sql .= "'" . cnv_sqlstr($beacon_identifier) . "',";
$sql .= " NULL ";
$sql .= ")";

$res = db_query($sql, $conn);

if ($res) {
$str = "ビーコン識別子に" . cnv_sqlstr($beacon_identifier) . "を追加したよ";
}
else {
$str = "登録失敗しちゃった…";
}

return $str;
}

//更新の処理
if ($kbn == "upd") {
$sql = "UPDATE beacon_identifier SET ";
$sql .= " beacon_identifier = '" . cnv_sqlstr($beacon_identifier) . "'";
$sql .= " WHERE beacon_identifier = " . cnv_sqlstr($beacon_identifier_old);

$res = db_query($sql, $conn);

if ($res) {
$str = "更新完了したよ";
}
else {
$str = "更新失敗しちゃった…";
}
return $str;
}

//削除の処理
if ($kbn == "del") {
$sql = "DELETE FROM beacon_identifier ";
$sql .= "WHERE beacon_identifier.beacon_identifier = '" . cnv_sqlstr($beacon_identifier) . "'";
$res = db_query($sql, $conn);
if ($res) {
return "消しておいたよ。";
}
else {
return "消すの失敗しちゃった…";
}
}

}

function db_close($conn) {
mysqli_close($conn);
}
?>