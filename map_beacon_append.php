<?php 

session_cache_limiter("public");
session_start();

require "roomconfig.php";

$prmarray = cnv_formstr($_POST);

if (isset($prmarray["act"])) {

$act = $prmarray["act"];

}

else {

$act = "ent";
}

date_default_timezone_set('Asia/Tokyo');
$dt = date("Y-m-d H:i:s");

?>
<?php $conn = db_conn(); ?>
<html>

<head>
<meta http—equiv="content—type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="style.css" type="text/css" />
<title>ビーコン設定</title>

</head>

<body bgcolor="lightyellow">

<div align="center">

<?php
call_user_func("screen_".$act, $prmarray);
?>

</div>

</body>

</html>

<?php db_close($conn); ?>

<?php 
function screen_ent($array) {
if (!isset($array["room_id"])) { return; }

$row = get_data($array["room_id"]);

?>

<h3>ビーコン追加</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>棟名</td>
<td><?=cnv_dispstr($row["building_name"])?></td>
</tr>
<tr>
<td>部屋番号</td>
<td><?=cnv_dispstr($row["roomnumber_no"])?></td>
</tr>
<tr>
<td>部屋名</td>
<td><?=cnv_dispstr($row["room_name"])?></td>
</tr>
</table>
<h>追加するビーコンの選択</h>
<table border="1">
<tr>
<td>ビーコンID</td>
<?php $sql = "SELECT * FROM beacon_identifier WHERE beacon_identifier.room_id is NULL ORDER BY beacon_identifier.beacon_identifier";
$selected = array();
 ?>
<td><?php select_option("beacon_identifier[]", "beacon_identifier", $sql, $selected, FALSE); ?></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="追加確認" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="building_name" value="<?=cnv_dispstr($row["building_name"])?>">
<input type="hidden" name="roomnumber_no" value="<?=cnv_dispstr($row["roomnumber_no"])?>">
<input type="hidden" name="room_name" value="<?=cnv_dispstr($row["room_name"])?>">
<input type="hidden" name="act" value="updconf">
<input type="hidden" name="room_id" value="<?=$row["room_id"]?>">
<input type="hidden" name="beacon_identifier_old" value="<?=$beacon_identifier_old?>">
</form>
<?php
}

function screen_updconf($row) {
 ?>

<h3>設置確認画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>棟名</td>
<td><?=$row["building_name"]?></td>
</tr>
<tr>
<td>部屋番号</td>
<td><?=$row["roomnumber_no"]?></td>
</tr>
<tr>
<td>部屋名</td>
<td><?=$row["room_name"]?></td>
</tr>
</table>
<h>設置するビーコン</h>
<table border="1">
<tr>
<td>ビーコンID</td>
<?php 
if(isset($_POST["beacon_identifier"])){
$beacon_identifier = implode(',',$_POST["beacon_identifier"]); 
}else{
	$beacon_identifier = null;
}
?>
<td><?=$beacon_identifier?></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="ビーコン追加実行" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="room_id" value="<?=$row["room_id"]?>">
<input type="hidden" name="beacon_identifier" value="<?=$beacon_identifier?>">
<input type="hidden" name="building_name" value="<?=$row["building_name"]?>">
<input type="hidden" name="roomnumber_no" value="<?=$row["roomnumber_no"]?>">
<input type="hidden" name="room_name" value="<?=$row["room_name"]?>">
<input type="hidden" name="act" value="dojob">
</form>
<?php
}

function screen_dojob($array) {
$res_mes = db_update($array);
?>

<h3>処理完了画面</h3>

<table border="0" bgcolor="pink">
<tr><th>処理完了</th></tr>
<tr><td><?=$res_mes; ?></td></tr>
</table>
<?php
}

function db_conn() {
	//mysqliに変更
	$conn = mysqli_connect(DBSV, DBUSER, DBPASS, DBNAME) or die("接続エラー");
	return $conn;
}

function db_close($conn) {
	mysqli_close($conn);
}

function get_data($room_id) {
global $conn;

$sql = "SELECT roomdata.room_id, building_name, roomnumber_no, room_name, GROUP_CONCAT(beacon_identifier SEPARATOR ',') as beacon_identifier FROM building, roomnumber, roomdata";
$sql .= " left outer join beacon_identifier on (roomdata.room_id = beacon_identifier.room_id)";
$sql .= " WHERE (roomdata.building_id = building.building_id AND roomdata.roomnumber_id = roomnumber.roomnumber_id)";
$sql .= " AND (roomdata.room_id = '".cnv_sqlstr($room_id)."')";
$sql .= " GROUP BY roomdata.room_id";
$res = db_query($sql, $conn) or die("データ抽出エラー");
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
return $row;
}

//セレクトボックスを作る関数
function select_option($name, $option_name, $sql, $selected, $multiple){
global $conn;
$res = db_query($sql, $conn) or die("データ抽出エラー");
?>
<select name="<?=$name?>" <?php if($multiple){?>multiple<?php } ?>>
<?php foreach($selected as $value){ ?>
<option selected><?=$value?></option>
<?php }?>
<?php while ($row = mysqli_fetch_array($res, MYSQL_ASSOC)) { ?>
<option><?=cnv_dispstr($row[$option_name])?></option>
<?php } ?>
</select>
<?php 
}

function cnv_dispstr($string) {
$det_enc = mb_detect_encoding($string, "UTF-8");
if ($det_enc and $det_enc != ENCDISP) {
return mb_convert_encoding($string, ENCDISP, $det_enc);

}
else {
return $string;
}
}

function cnv_formstr($array) {

foreach($array as $k => $v){
if (get_magic_quotes_gpc()) {
$v = stripslashes($v);
}
$v = htmlspecialchars($v);
$array[$k] = $v;
}
return $array;
}

function cnv_sqlstr($string) {
$det_enc = mb_detect_encoding($string,"UTF-8");
if ($det_enc and $det_enc != ENCDB) {
$string = mb_convert_encoding($string, ENCDB, $det_enc);
}

$string = addslashes($string);
return $string;
}

function db_query($sql, $conn) {
$res = mysqli_query($conn, $sql);
return $res;
}

function db_update($array) {
global $conn;

$room_id = $array["room_id"];
$beacon_identifier = $array["beacon_identifier"];
$sql = "UPDATE beacon_identifier SET ";
$sql .= " room_id = '" . cnv_sqlstr($room_id) . "'";
$sql .= "WHERE beacon_identifier.beacon_identifier = '" . cnv_sqlstr($beacon_identifier) . "'";
$res = db_query($sql, $conn);

if ($res) {
$str = "ビーコンの追加が完了したよ。";
}
else {
$str = "ビーコンの追加、失敗しちゃった…";
}
return $str;
}

 ?>