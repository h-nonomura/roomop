<?php
/////////////////
//入力仕様上の注意//
/////////////////
//部屋名、棟名、部屋番号はNULL不可なので特に空き部屋の場合は"空き部屋"と記述すること
//csvの形式はカンマ区切りで棟,部屋番号,部屋名,識別子、と記述すること
//識別子を設定しない場合は"なし"と記述すること
//NULLでも空白を入れたりしないこと
//識別子を複数登録したい場合はカンマ(,)で区切らず、縦線(|)で区切ること
//既に使われている識別子を別の部屋に割り当てて書いた場合は前に指定されていた部屋の割り当てを解除して新しい部屋に割り当てられます
session_cache_limiter("public");
session_start();

require "roomconfig.php";

$prmarray = cnv_formstr($_POST);

if (isset($prmarray["act"])) {

$act = $prmarray["act"];

}

else {

$act = DEFSCR;
}

date_default_timezone_set('Asia/Tokyo');
$dt = date("Y-m-d H:i:s");

?>
<?php $conn = db_conn(); ?>
<html>

<head>
<meta http—equiv="content—type" content="text/html; charset=utf-8">
<link rel="stylesheet" href="style.css" type="text/css" />
<title><?=ADMINAPPNAME?></title>

</head>

<body bgcolor="lightyellow">

<div align="center">

<?php
call_user_func("screen_".$act, $prmarray);
?>

</div>

</body>

</html>

<?php db_close($conn); ?>

<?php

function screen_src($array) {
$key = (isset($array["key"])) ? $array["key"] : "";
$order = (isset($array["order"])) ? $array["order"] : "roomdata.room_id";
$ascdesc = (isset($array["ascdesc"])) ? $array["ascdesc"] : "ASC";
$p = (isset($array["p"])) ? intval($array["p"]) : 1;
$p = ($p < 1) ? 1 : $p;

?>

<?php disp_menu(); ?>
<?php disp_csvup(); ?>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="0">
<tr>
<td><input type="text" name="key" value="<?=$key?>"></td>
<td><select name="order">
	<option value="roomdata.room_id" <?php if($order === "roomdata.room_id"){ ?> selected <?php } ?>>登録順</option>
	<option value="building.building_name" <?php if($order === "building.building_name"){ ?> selected <?php } ?>>棟名順</option>
	<option value="roomnumber.roomnumber_no" <?php if($order === "roomnumber.roomnumber_no"){ ?> selected <?php } ?>>部屋番号順</option>
	<option value="roomdata.room_name" <?php if($order === "roomdata.room_name"){ ?> selected <?php } ?>>部屋名順</option>
	<option value="beacon_identifier.beacon_identifier" <?php if($order === "beacon_identifier.beacon_identifier"){ ?> selected <?php } ?>>識別子順</option>
</select></td>
<td><select name="ascdesc">
	<option value="asc" <?php if($ascdesc === "asc"){ ?> selected <?php } ?>>昇順</option>
	<option value="desc" <?php if($ascdesc === "desc"){ ?> selected <?php } ?>>降順</option>
</select></td>
<td><input type="submit" value="検索" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="act" value="src">
</form>
<?php disp_listdata($key, $p, $order, $ascdesc); ?>
<?php
}

function screen_ent() {

?>

<?php disp_menu(); ?>
<h3>登録画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>棟名</td>
<?php 
$sql = "SELECT * FROM building ORDER BY building.building_id";
$selected = array();
?>
<td><?php select_option("building_name", "building_name", $sql, $selected, FALSE); ?></td>
</tr>
<tr>
<td>部屋番号</td>
<?php 
$sql = "SELECT * FROM roomnumber ORDER BY roomnumber.roomnumber_id";
$selected = array();
?>
<td><?php select_option("roomnumber_no", "roomnumber_no", $sql, $selected, FALSE); ?></td>
</tr>
<tr>
<td>部屋名</td>
<td><input type="text" name="room_name" size="50"></td>
</tr>
<tr>
<td>識別子</td>
<?php 
$sql = "SELECT * FROM beacon_identifier WHERE beacon_identifier.room_id is NULL ORDER BY beacon_identifier.beacon_identifier";
$selected = array();
?>
<td><?php select_option("beacon_identifier[]", "beacon_identifier", $sql, $selected, TRUE); ?></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="登録確認" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="act" value="entconf">
</form>
<?php
}
function screen_entconf($array) {
if (!chk_data($array)) { return; }
extract($array);
?>

<?php disp_menu(); ?>
<h3>登録確認画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>棟名</td>
<td><?=$building_name?></td>
</tr>
<tr>
<td>部屋番号</td>
<td><?=$roomnumber_no?></td>
</tr>
<tr>
<td>部屋名</td>
<td><?=$room_name?></td>
</tr>
<tr>
<td>識別子</td>
<?php 
if(isset($_POST["beacon_identifier"])){
$beacon_identifier = implode(',',$_POST["beacon_identifier"]); 
}else{
	$beacon_identifier = null;
}
?>
<td><?=$beacon_identifier?></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="登録実行" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="beacon_identifier" value="<?=$beacon_identifier?>">
<input type="hidden" name="building_name" value="<?=$building_name?>">
<input type="hidden" name="roomnumber_no" value="<?=$roomnumber_no?>">
<input type="hidden" name="room_name" value="<?=$room_name?>">
<input type="hidden" name="act" value="dojob">
<input type="hidden" name="kbn" value="ent">
</form>
<?php
}

function screen_upd($array) {
if (!isset($array["room_id"])) { return; }

$row = get_data($array["room_id"]);

$beacon_identifier_old = $row["beacon_identifier"];
?>

<?php disp_menu(); ?>

<h3>更新画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>棟名</td>
<?php $sql = "SELECT * FROM building ORDER BY building.building_id"?>
<td><?php select_option("building_name", "building_name", $sql, array(cnv_dispstr($row["building_name"])), FALSE); ?></td>
</tr>
<tr>
<td>部屋番号</td>
<?php $sql = "SELECT * FROM roomnumber ORDER BY roomnumber.roomnumber_id"?>
<td><?php select_option("roomnumber_no", "roomnumber_no", $sql, array(cnv_dispstr($row["roomnumber_no"])), FALSE); ?></td>
</tr>
<tr>
<td>部屋名</td>
<td><input type="text" name="room_name"
value="<?=cnv_dispstr($row["room_name"])?>" size="50"></td>
</tr>
<tr>
<td>識別子</td>
<?php $sql = "SELECT * FROM beacon_identifier WHERE beacon_identifier.room_id is NULL ORDER BY beacon_identifier.beacon_identifier"?>
<td><?php select_option("beacon_identifier[]", "beacon_identifier", $sql, explode(',', $row["beacon_identifier"]), TRUE); ?></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="更新確認" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="act" value="updconf">
<input type="hidden" name="room_id" value="<?=$row["room_id"]?>">
<input type="hidden" name="beacon_identifier_old" value="<?=$beacon_identifier_old?>">
</form>
<?php
}

function screen_updconf($row) {

if (!chk_data($row)) { return; }
?>

<?php disp_menu(); ?>
<h3>更新確認画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>棟名</td>
<td><?=$row["building_name"]?></td>
</tr>
<tr>
<td>部屋番号</td>
<td><?=$row["roomnumber_no"]?></td>
</tr>
<tr>
<td>部屋名</td>
<td><?=$row["room_name"]?></td>
</tr>
<tr>
<td>識別子</td>
<?php 
if(isset($_POST["beacon_identifier"])){
$beacon_identifier = implode(',',$_POST["beacon_identifier"]); 
}else{
	$beacon_identifier = null;
}
?>
<td><?=$beacon_identifier?></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="更新実行" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="room_id" value="<?=$row["room_id"]?>">
<input type="hidden" name="beacon_identifier_old" value="<?=$row["beacon_identifier_old"]?>">
<input type="hidden" name="beacon_identifier" value="<?=$beacon_identifier?>">
<input type="hidden" name="building_name" value="<?=$row["building_name"]?>">
<input type="hidden" name="roomnumber_no" value="<?=$row["roomnumber_no"]?>">
<input type="hidden" name="room_name" value="<?=$row["room_name"]?>">
<input type="hidden" name="act" value="dojob">
<input type="hidden" name="kbn" value="upd">
</form>
<?php
}

function screen_delconf($array) {
if (!isset($array["room_id"])) { return; }
$row = get_data($array["room_id"]);

?>

<?php disp_menu(); ?>
<h3>削除確認画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>棟名</td>
<td><?=$row["building_name"]?></td>
</tr>
<tr>
<td>部屋番号</td>
<td><?=$row["roomnumber_no"]?></td>
</tr>
<tr>
<td>部屋名</td>
<td><?=$row["room_name"]?></td>
</tr>
<tr>
<td>識別子</td>
<td><?=$row["beacon_identifier"]?></td>
</tr>
<tr>
<td> </td>
<td><input type="submit" value="削除実行" name="sub1"></td>
</tr>
</table>
<input type="hidden" name="room_id" value="<?=$row["room_id"]?>">
<input type="hidden" name="act" value="dojob">
<input type="hidden" name="kbn" value="del">
</form>
<?php
}

function screen_dojob($array) {
$res_mes = db_update($array);
?>

<p><?php disp_menu(); ?>
<h3>処理完了画面</h3>

<table border="0" bgcolor="pink">
<tr><th>処理完了</th></tr>
<tr><td><?=$res_mes; ?></td></tr>
</table>
<?php
}

function screen_csvent() {

//改行コードはUNIXで
//

$file = $_FILES["upfile"]["tmp_name"];
$data = file_get_contents($file);
$data = mb_convert_encoding($data, 'UTF-8', 'sjis');
//$data = mb_convert_encoding($data, 'UTF-8', 'auto');
$data = str_replace(array("\x0d\x0a", "\x0a", "\x0d"), PHP_EOL, $data);
$temp = tmpfile();
$csv  = array();
 
fwrite($temp, $data);
rewind($temp);

//hidden渡しに利用
$csvdata = $data;

//確認用
while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
    $csv[] = $data;
}
fclose($temp);
?>

<?php disp_menu(); ?>
<h3>csv登録確認画面</h3>

<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<table border="1">
<tr>
<td>行数</td>
<td>棟名</td>
<td>部屋番号</td>
<td>部屋名</td>
<td>識別子</td>
</tr>
<?php foreach($csv as $key=>$val) { ?>
<tr>
<td>
<table>
<tr><?=$key?></tr>
</table>
</td>
<?php foreach($val as $key2=>$val2) { ?>
        <td><?=cnv_dispstr($val2)?></td>
<?php } ?>
</tr>
<?php } ?>
</table>
<table>
<tr><td title="チェックすると同じ棟のデータを全て消去してからデータ入力が行われます"><input type="checkbox" name="overwrite" value="TRUE">上書きする</td></tr>
<tr><td align="center"><input type="submit" value="登録実行" name="sub1"></td></tr>
<input type="hidden" name="csvdata" value="<?=$csvdata?>">
<input type="hidden" name="act" value="dojob">
<input type="hidden" name="kbn" value="csvent">
</table>
</form>

<?php
}

function chk_data($array) {

$strerr = "";

//if ($array["beacon_identifier"] == "") {
//echo "<p>識別子が入力されていません";
//$strerr = "1";
//}
if ($array["building_name"] == "") {
echo "棟名が入力されてないよ<br>";
$strerr = "1";
}
if ($array["roomnumber_no"] == "") {
echo "部屋番号が入力されていないよ<br>";
$strerr = "1";
}
if ($array["room_name"] == "") {
echo "部屋名が入力されいないよ<br>";
$strerr = "1";
}
if ($strerr == "1") {
return FALSE; 
}
else {

return TRUE;
}
}

function cnv_formstr($array) {

foreach($array as $k => $v){
if (get_magic_quotes_gpc()) {
$v = stripslashes($v);
}
$v = htmlspecialchars($v);
$array[$k] = $v;
}
return $array;
}

function cnv_sqlstr($string) {
$det_enc = mb_detect_encoding($string,"UTF-8");
if ($det_enc and $det_enc != ENCDB) {
$string = mb_convert_encoding($string, ENCDB, $det_enc);
}

$string = addslashes($string);
return $string;
}

function cnv_dispstr($string) {
$det_enc = mb_detect_encoding($string, "UTF-8");
if ($det_enc and $det_enc != ENCDISP) {
return mb_convert_encoding($string, ENCDISP, $det_enc);

}
else {
return $string;
}
}

function cnv_link($url, $title) {
$string = "<a href=\"$url\">".$title."</a>";
return $string;
}

function get_data($room_id) {
global $conn;

$sql = "SELECT roomdata.room_id, building_name, roomnumber_no, room_name, GROUP_CONCAT(beacon_identifier SEPARATOR ',') as beacon_identifier FROM building, roomnumber, roomdata";
$sql .= " left outer join beacon_identifier on (roomdata.room_id = beacon_identifier.room_id)";
$sql .= " WHERE (roomdata.building_id = building.building_id AND roomdata.roomnumber_id = roomnumber.roomnumber_id)";
$sql .= " AND (roomdata.room_id = '".cnv_sqlstr($room_id)."')";
$sql .= " GROUP BY roomdata.room_id";
$res = db_query($sql, $conn) or die("データ抽出エラー");
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
return $row;
}

//セレクトボックスを作る関数
function select_option($name, $option_name, $sql, $selected, $multiple){
global $conn;
$res = db_query($sql, $conn) or die("データ抽出エラー");
?>
<select name="<?=$name?>" <?php if($multiple){?>multiple<?php } ?>>
<?php foreach($selected as $value){ ?>
<option selected><?=$value?></option>
<?php }?>
<?php while ($row = mysqli_fetch_array($res, MYSQL_ASSOC)) { ?>
<option><?=cnv_dispstr($row[$option_name])?></option>
<?php } ?>
</select>
<?php 
}

function disp_listdata($key, $p, $order, $ascdesc) {

global $conn;

$st = ($p - 1) * intval(ADMINPAGESIZE);

$sql = "SELECT roomdata.room_id, building_name, roomdata.building_id, roomnumber_no, room_name, GROUP_CONCAT(beacon_identifier SEPARATOR ',') as beacon_identifier FROM building, roomnumber, roomdata";
$sql .= " left outer join beacon_identifier on (roomdata.room_id = beacon_identifier.room_id)";
$sql .= " WHERE (roomdata.building_id = building.building_id AND roomdata.roomnumber_id = roomnumber.roomnumber_id)";
if (strlen($key) > 0) {
$sql .= " AND ((building_name LIKE '%".cnv_sqlstr($key)."%')";
$sql .= " OR (roomnumber_no LIKE '%".cnv_sqlstr($key)."%')";
$sql .= " OR (room_name LIKE '%".cnv_sqlstr($key)."%'))";
}
$sql .= " GROUP BY roomdata.room_id";
$sql .= " ORDER BY " . cnv_sqlstr($order) . " " . cnv_sqlstr($ascdesc);
$res = db_query($sql, $conn) or die("データ抽出エラー");
if (mysqli_num_rows($res) <= 0) {
echo "<p>データは登録されていません";
return;
}
?>

<table border="1">
<tr>
<td> </td>
<td>棟名</td>
<td>部屋番号</td>
<td>部屋名</td>
<td>識別子</td>
</tr>
<?php while ($row = mysqli_fetch_array($res, MYSQL_ASSOC)) { ?>
<tr>
<td>
<table>
<tr>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td><input type="submit" value="更新"></td>
<input type="hidden" name="act" value="upd">
<input type="hidden" name="room_id" value="<?=$row["room_id"]?>">
</form>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td width="50%"><input type="submit" value="削除"></td>
<input type="hidden" name="act" value="delconf">
<input type="hidden" name="room_id" value="<?=$row["room_id"]?>">
</form>
<form method="POST" action="<?php echo "building_map_" . $row["building_id"] . ".php"?>">
<td width="50%"><input type="submit" value="マップ"></td>
<input type="hidden" name="roomnumber_no" value="<?=$row["roomnumber_no"]?>">
</form>
</tr>
</table>
</td>
<td><?=cnv_dispstr($row["building_name"])?></td>
<td><?=cnv_dispstr($row["roomnumber_no"])?></td>
<td><?=cnv_dispstr($row["room_name"])?></td>
<td><?=cnv_dispstr($row["beacon_identifier"])?></td>
</tr>
<?php } ?>
</table>

<?php disp_pagenav($key, $p, $order, $ascdesc); ?>
<?php
}

function disp_menu() {
?>
<table border="1">
<tr>
<th colspan="8"><big><b><?=ADMINAPPNAME?></b></big></th>
</tr>
<tr>
<form method="POST" action="roomop.php">
<td><input type="submit" value="部屋の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="roomop.php">
<td><input type="submit" value="部屋の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
<tr>
<form method="POST" action="buildingop.php">
<td><input type="submit" value="棟の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="buildingop.php">
<td><input type="submit" value="棟の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
<tr>
<form method="POST" action="roomnumberop.php">
<td><input type="submit" value="部屋番号の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="roomnumberop.php">
<td><input type="submit" value="部屋番号の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
<tr>
<form method="POST" action="beacon_identifierop.php">
<td><input type="submit" value="識別子の登録画面へ"></td>
<input type="hidden" name="act" value="ent">
</form>
<form method="POST" action="beacon_identifierop.php">
<td><input type="submit" value="識別子の検索画面へ"></td>
<input type="hidden" name="act" value="src">
</form>
</tr>
</table>
<?php 
}

function disp_pagenav($key, $p = 1, $order, $ascdesc) {
global $conn;
$prev = $p - 1;
$prev = ($prev < 1) ? 1 : $prev;
$next = $p + 1;

$sql = "SELECT COUNT(roomdata.room_id) as cnt, building_name, roomnumber_no, room_name, GROUP_CONCAT(beacon_identifier SEPARATOR ',') as beacon_identifier FROM building, roomnumber, roomdata";
$sql .= " left outer join beacon_identifier on (roomdata.room_id = beacon_identifier.room_id)";
$sql .= " WHERE (roomdata.building_id = building.building_id AND roomdata.roomnumber_id = roomnumber.roomnumber_id)";
if (isset($key)) {
if (strlen($key) > 0) {
$sql .= " AND ((building_name LIKE '%".cnv_sqlstr($key)."%')";
$sql .= " OR (roomnumber_no LIKE '%".cnv_sqlstr($key)."%')";
$sql .= " OR (room_name LIKE '%".cnv_sqlstr($key)."%'))";
}
}
$sql .= " GROUP BY roomdata.room_id";
$sql .= " ORDER BY " . cnv_sqlstr($order) . " " . cnv_sqlstr($ascdesc);
$res = db_query($sql, $conn) or die("データ抽出エラー");
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
$recordcount = $row["cnt"];
?>

<table>
<tr>
<?php if ($p > 1) { ?>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td><input type="submit" value="<< 前"></td>
<input type="hidden" name="act" value="src">
<input type="hidden" name="p" value="<?=$prev?>">
<input type="hidden" name="key" value="<?=$key?>">
<input type="hidden" name="order" value="<?=$order?>">
<input type="hidden" name="ascdesc" value="<?=$ascdesc?>">
</form>
<?php } ?>
<?php if ($recordcount > ($next - 1) * intval(ADMINPAGESIZE)) { ?>
<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
<td width="50%"><input type="submit" value="次 >>"></td>
<input type="hidden" name="act" value="src">
<input type="hidden" name="p" value="<?=$next?>">
<input type="hidden" name="key" value="<?=$key?>">
<input type="hidden" name="order" value="<?=$order?>">
<input type="hidden" name="ascdesc" value="<?=$ascdesc?>">
</form>
<?php } ?>
</tr>
</table>
<?php
}

function disp_csvup() { ?>

<form action="<?=$_SERVER["PHP_SELF"]?>" method="post" enctype="multipart/form-data">
<table border="0">
<tr>csvファイルで登録</tr>
<tr>
<td><input type="file" name="upfile" size="30" /></td>
<td><input type="submit" value="送信" /></td>
</table>
<input type="hidden" name="act" value="csvent">
</form>
<?php }

function db_conn() {
//mysqliに変更
$conn = mysqli_connect(DBSV, DBUSER, DBPASS, DBNAME) or die("接続エラー");
return $conn;
}

function db_query($sql, $conn) {
$res = mysqli_query($conn, $sql);
return $res;
}

function db_update($array) {
global $conn;
global $dt;
if (!isset($array["kbn"])) { return "パラメータエラー"; }
if($array["kbn"] == "csvent"){
	$data = $array["csvdata"];
	$temp = tmpfile();
	$csv  = array();
	$err_flg = array();
	
	fwrite($temp, $data);
	rewind($temp);
	
	while (($data = fgetcsv($temp, 0, ",")) !== FALSE) {
		$csv[] = $data;
	}
	fclose($temp);
	
	//非ウィンドウバージョン
	//とりあえず表示だけ消しておく
	?> <p><span style="display:none;"> <?php
	foreach($csv as $key=>$val) {
		echo $key . "行目…";
		if(chk_data(array("building_name" => $val[0], "roomnumber_no" => $val[1], "room_name" => $val[2]))){
			echo "問題ないよ";
			$err_flg[] = 0;
		}else{
			$err_flg[] = 1;
		}
	} ?> </span></p> <?php
}else{
	if ($array["kbn"] != "del") {
		if (!chk_data($array)) { return "パラメータエラー"; }
	}
	if ($array["kbn"] != "ent") {
		if (!isset($array["room_id"])) { return "パラメータエラー"; }
	}
}

extract($array);

////////////
//登録の処理//
////////////
if ($kbn == "ent") {

//棟IDを取得
$sql = "SELECT * FROM building ";
$sql .= " WHERE (building.building_name LIKE '%".cnv_sqlstr($building_name)."%')";

$res = db_query($sql, $conn);
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
$building_id = $row["building_id"];

//部屋番号IDを取得
$sql = "SELECT * FROM roomnumber ";
$sql .= " WHERE (roomnumber.roomnumber_no LIKE '%".cnv_sqlstr($roomnumber_no)."%')";

$res = db_query($sql, $conn);
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
$roomnumber_id = $row["roomnumber_id"];

//部屋のダブりがないかをチェック
$sql = "SELECT * FROM roomdata ";
$sql .= " WHERE (roomdata.roomnumber_id LIKE '%".cnv_sqlstr($roomnumber_id)."%')";
$sql .= " AND (roomdata.building_id LIKE '%".cnv_sqlstr($building_id)."%')";
$res = db_query($sql, $conn);
if (mysqli_num_rows($res) != 0) {
return "同じ棟の同じ部屋番号の所に二つの部屋は設定できないよ。";
}

//roomdataの登録
$sql = "INSERT INTO roomdata (";
$sql .= " building_id, ";
$sql .= " roomnumber_id, ";
$sql .= " room_name ";
$sql .= ") VALUES (";
$sql .= "'" . cnv_sqlstr($building_id) . "',";
$sql .= "'" . cnv_sqlstr($roomnumber_id) . "',";
$sql .= "'" . cnv_sqlstr($room_name) . "'";
$sql .= ")";
$res1 = db_query($sql, $conn);

//登録されたroomdataのroom_idを調べる
$sql = "SELECT * FROM roomdata ";
$sql .= " WHERE (roomdata.roomnumber_id LIKE '%".cnv_sqlstr($roomnumber_id)."%')";
$sql .= " AND (roomdata.building_id LIKE '%".cnv_sqlstr($building_id)."%')";
$res = db_query($sql, $conn);
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
$room_id = $row["room_id"];

//room_idをbeacon_identifierにセット
$beacon_identifier = explode(',', $beacon_identifier);
foreach($beacon_identifier as $value){
$sql = "UPDATE beacon_identifier SET ";
$sql .= " room_id = '" . cnv_sqlstr($room_id) . "'";
$sql .= "WHERE beacon_identifier.beacon_identifier = '" . cnv_sqlstr($value) . "'";
$res2 = db_query($sql, $conn);
}

$res = $res1*$res2;

if ($res) {
$str = "登録完了したよ。";
}
else {
$str = "登録失敗しちゃった…";
}
return $str;
}

/////////////////////
//csvによる登録の処理//
////////////////////

if ($kbn == "csvent") {
	$str = "";
	if(isset($overwrite)){
		
		//棟のIDを取得
		$sql = "SELECT * FROM building";
		$sql .= " WHERE (building.building_name LIKE '%".cnv_sqlstr($csv[0][0])."%')";
		$res = db_query($sql, $conn);
		$row = mysqli_fetch_array($res, MYSQL_ASSOC);
		$building_id = $row["building_id"];
		
		
		while(1){
			//消滅させるroom_idを検出
			$sql = "SELECT * FROM roomdata";
			$sql .= " WHERE (roomdata.building_id = ".$building_id.")";
			$res = db_query($sql, $conn);
			if (mysqli_num_rows($res) === 0){
				break;
			}
			$row = mysqli_fetch_array($res, MYSQL_ASSOC);
			$room_id = $row["room_id"];
			
			//検出したroom_idのとこのデータを消す
			$sql = "UPDATE beacon_identifier SET room_id = NULL ";
			$sql .= "WHERE beacon_identifier.room_id = '" . cnv_sqlstr($room_id) . "'";
			$res2 = db_query($sql, $conn);
			$sql = "DELETE FROM roomdata ";
			$sql .= "WHERE roomdata.room_id = '" . cnv_sqlstr($room_id) . "'";
			$res1 = db_query($sql, $conn);
		}
		$str .= "<p>棟の情報が上書きされたよ";
	}
	
	foreach($csv as $key=>$val) {
		if($err_flg[$key] == 0){
			
			$chk = "";
			$building_name = $val[0];
			$roomnumber_no = $val[1];
			$room_name = $val[2];
			$beacon_identifier = explode('|', $val[3]);
			
			//棟を登録
			$sql = "INSERT INTO building (";
			$sql .= " building_id, ";
			$sql .= " building_name ";
			$sql .= ") VALUES (";
			$sql .= " NULL, ";
			$sql .= "'" . cnv_sqlstr($building_name) . "'";
			$sql .= ")";
			
			$res = db_query($sql, $conn);
			
			if ($res) {
				$str .= "<p>建物に" . cnv_sqlstr($building_name) . "を追加したよ";
			} else {
				$str .= "";
			}
			
			//部屋番号を登録
			
			$sql = "INSERT INTO roomnumber (";
			$sql .= " roomnumber_id, ";
			$sql .= " roomnumber_no ";
			$sql .= ") VALUES (";
			$sql .= " NULL, ";
			$sql .= "'" . cnv_sqlstr($roomnumber_no) . "'";
			$sql .= ")";
			
			$res = db_query($sql, $conn);
			
			if ($res) {
				$str .= "<p>部屋番号に" . cnv_sqlstr($roomnumber_no) . "を追加したよ";
			} else {
				$str .= "";
			}
			
			//ビーコン識別子を追加
			
			foreach($beacon_identifier as $value){
				if($value != "なし"){
					$sql = "INSERT INTO beacon_identifier (";
					$sql .= " beacon_identifier, ";
					$sql .= " room_id ";
					$sql .= ") VALUES (";
					$sql .= "'" . $value . "',";
					$sql .= " NULL ";
					$sql .= ")";
					
					$res = db_query($sql, $conn);
					
					if ($res) {
						$str .= "<p>ビーコン識別子に" . $value . "を追加したよ";
					} else {
						$str .= "";
					}
				}
			}
			
			//棟IDを取得
			$sql = "SELECT * FROM building ";
			$sql .= " WHERE (building.building_name LIKE '%".cnv_sqlstr($building_name)."%')";
			$res = db_query($sql, $conn);
			$row = mysqli_fetch_array($res, MYSQL_ASSOC);
			$building_id = $row["building_id"];
			
			if(!$building_id){
				$chk .= "<p>棟IDの取得に失敗";
			}
			
			//部屋番号IDを取得
			$sql = "SELECT * FROM roomnumber ";
			$sql .= " WHERE (roomnumber.roomnumber_no LIKE '%".cnv_sqlstr($roomnumber_no)."%')";
			
			$res = db_query($sql, $conn);
			$row = mysqli_fetch_array($res, MYSQL_ASSOC);
			$roomnumber_id = $row["roomnumber_id"];
			
			if(!$roomnumber_id){
				$chk .= "<p>部屋番号IDの取得に失敗";
			}
			
			//部屋のダブりがないかをチェック(あったら更新)
			$ccd = FALSE;
			
			$sql = "SELECT * FROM roomdata ";
			$sql .= " WHERE (roomdata.roomnumber_id LIKE '%".cnv_sqlstr($roomnumber_id)."%')";
			$sql .= " AND (roomdata.building_id LIKE '%".cnv_sqlstr($building_id)."%')";
			$res = db_query($sql, $conn);
			if (mysqli_num_rows($res) != 0) {
				$ccd = TRUE;
				$row = mysqli_fetch_array($res, MYSQL_ASSOC);
				$room_name_old = $row["room_name"];
			}
			
			//roomdataの登録(更新)
			if(!$ccd){
				//ダブりがなかった時(新規登録)
				$sql = "INSERT INTO roomdata (";
				$sql .= " building_id, ";
				$sql .= " roomnumber_id, ";
				$sql .= " room_name ";
				$sql .= ") VALUES (";
				$sql .= "'" . cnv_sqlstr($building_id) . "',";
				$sql .= "'" . cnv_sqlstr($roomnumber_id) . "',";
				$sql .= "'" . cnv_sqlstr($room_name) . "'";
				$sql .= ")";
				$res = db_query($sql, $conn);
				
				if($res){
					$str .= "<p>" . cnv_sqlstr($building_name) . "、" . cnv_sqlstr($roomnumber_no) . "、" . cnv_sqlstr($room_name) . "を新しく登録したよ";
				} else {
					$str .= "<p>登録失敗しちゃった…";
					break;
				}
			} 
			
			//登録されたroomdataのroom_idを調べる
			$sql = "SELECT * FROM roomdata ";
			$sql .= " WHERE (roomdata.roomnumber_id LIKE '%".cnv_sqlstr($roomnumber_id)."%')";
			$sql .= " AND (roomdata.building_id LIKE '%".cnv_sqlstr($building_id)."%')";
			$res = db_query($sql, $conn);
			$row = mysqli_fetch_array($res, MYSQL_ASSOC);
			$room_id = $row["room_id"];
			
			if(!$room_id){
				$chk .= "<p>room_idの取得に失敗";
			}
			
			if($ccd){
				//ダブりがあった時(更新)
				//roomdataの更新
				
				if($room_name_old != $room_name){
					
					$sql = "UPDATE roomdata SET";
					$sql .= " building_id = '" . cnv_sqlstr($building_id) . "',";
					$sql .= " roomnumber_id = '" . cnv_sqlstr($roomnumber_id) . "',";
					$sql .= " room_name = '" . cnv_sqlstr($room_name) . "'";
					$sql .= " WHERE room_id = '" . cnv_sqlstr($room_id) . "'";
					$res = db_query($sql, $conn);
					
					if($res){
						$str .= "<p>" . cnv_sqlstr($building_name) . "、" . cnv_sqlstr($roomnumber_no) . "の部屋名を" . cnv_sqlstr($room_name) . "に更新したよ";
					} else {
						$str .= "<p>更新失敗しちゃった…";
						break;
					}
				}
				
				//解除するbeacon_identifierの検索
				$rows = array();
				$sql = "SELECT beacon_identifier FROM beacon_identifier ";
				$sql .= " WHERE beacon_identifier.room_id = '" . cnv_sqlstr($room_id) . "'";
				$res = db_query($sql, $conn);
				while($row = mysqli_fetch_array($res, MYSQL_ASSOC)){
					$rows[] = $row;
				}
				
				//beacon_identifierの設定を解除
				foreach($rows as $value){
					$sql = "UPDATE beacon_identifier SET ";
					$sql .= " room_id = NULL";
					$sql .= " WHERE beacon_identifier.beacon_identifier = '" . $value["beacon_identifier"] . "'";
					db_query($sql, $conn);
				}
			}
			
			//room_idをbeacon_identifierにセット
			foreach($beacon_identifier as $value){
				if($value != "なし"){
					$sql = "UPDATE beacon_identifier SET ";
					$sql .= " room_id = '" . cnv_sqlstr($room_id) . "'";
					$sql .= "WHERE beacon_identifier.beacon_identifier = '" . $value . "'";
					$res = db_query($sql, $conn);
					
					if($res){
						$str .= "<p>" . cnv_sqlstr($building_name) . "、" . cnv_sqlstr($roomnumber_no) . "、" . cnv_sqlstr($room_name) . "に識別子" . $value . "のビーコンを設定したよ";
					}
					
					if(!$res){
						$chk .= "<p>beacon識別子の設定に失敗";
					}
				}
			}
			
			if($chk){
				echo $chk;
				break;
			}
			
		} else {
			$str .= "<p>" . $key . "行目、(" . $val[0] . "," . $val[1] . "," . $val[2] . "," . $val[3] . ")はデータ入力に不備があったから登録されなかったよ";
		}
	}

return $str;

}

////////////
//更新の処理//
////////////
if ($kbn == "upd") {

//棟IDを取得
$sql = "SELECT * FROM building ";
$sql .= " WHERE (building.building_name = '".cnv_sqlstr($building_name)."')";

$res = db_query($sql, $conn);
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
$building_id = $row["building_id"];

//部屋番号IDを取得
$sql = "SELECT * FROM roomnumber ";
$sql .= " WHERE (roomnumber.roomnumber_no = '".cnv_sqlstr($roomnumber_no)."')";

$res = db_query($sql, $conn);
$row = mysqli_fetch_array($res, MYSQL_ASSOC);
$roomnumber_id = $row["roomnumber_id"];

//部屋のダブりがないかをチェック
$sql = "SELECT * FROM roomdata ";
$sql .= " WHERE (roomdata.roomnumber_id = '".cnv_sqlstr($roomnumber_id)."')";
$sql .= " AND (roomdata.building_id = '".cnv_sqlstr($building_id)."')";
$sql .= " AND (roomdata.room_id != '".cnv_sqlstr($room_id)."')";
$res = db_query($sql, $conn);
if (mysqli_num_rows($res) != 0) {
return "同じ棟の同じ部屋番号の所に二つの部屋は設定できないよ。";
}

//roomdataの更新
$sql = "UPDATE roomdata SET";
$sql .= " building_id = '" . cnv_sqlstr($building_id) . "',";
$sql .= " roomnumber_id = '" . cnv_sqlstr($roomnumber_id) . "',";
$sql .= " room_name = '" . cnv_sqlstr($room_name) . "'";
$sql .= " WHERE room_id = '" . cnv_sqlstr($room_id) . "'";
$res1 = db_query($sql, $conn);

//beacon_identifierの設定を解除
$beacon_identifier_old = explode(',', $beacon_identifier_old);
foreach($beacon_identifier_old as $value){
$sql = "UPDATE beacon_identifier SET ";
$sql .= " room_id = NULL";
$sql .= " WHERE beacon_identifier.beacon_identifier = '" . cnv_sqlstr($value) . "'";
db_query($sql, $conn);
}

//room_idをbeacon_identifierにセット
$beacon_identifier = explode(',', $beacon_identifier);
foreach($beacon_identifier as $value){
$sql = "UPDATE beacon_identifier SET ";
$sql .= " room_id = '" . cnv_sqlstr($room_id) . "'";
$sql .= " WHERE beacon_identifier.beacon_identifier = '" . cnv_sqlstr($value) . "'";
$res2 = db_query($sql, $conn);
}

$res = $res1*$res2;

if ($res) {
$str = "roomdataの更新完了したよ。";
}
else {
$str = "roomdataの更新失敗しちゃった…";
}
return $str;
}

//削除の処理
if ($kbn == "del") {
$sql = "UPDATE beacon_identifier SET room_id = NULL ";
$sql .= "WHERE beacon_identifier.room_id = '" . cnv_sqlstr($room_id) . "'";
$res2 = db_query($sql, $conn);
$sql = "DELETE FROM roomdata ";
$sql .= "WHERE roomdata.room_id = '" . cnv_sqlstr($room_id) . "'";
$res1 = db_query($sql, $conn);
$res = $res1*$res2;
if ($res) {
return "消しておいたよ。";
}
else {
return "消すの失敗しちゃった…";
}
}

}

function db_close($conn) {
mysqli_close($conn);
}
?>