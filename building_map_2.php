<!-- Created with Inkscape (http://www.inkscape.org/) -->
<?php 
session_cache_limiter("public");
session_start();

require "roomconfig.php";

date_default_timezone_set('Asia/Tokyo');
$dt = date("Y-m-d H:i:s");

$building_name = "4号館別館";
$strong_roomnumber_no = $_POST["roomnumber_no"];
$font_size = 10;
$icon_size = 40;
$room_no_array = array("100","101","102","103","104","105","106","108","109",
					   "200","201","202","203","204","205","206","207","209","212","213",
					   "300","301","302","303","304","305","306","307","310","311",
					   "400","401","402","403","404","405","406","407","408","409");

?>
<?php $conn = db_conn(); ?>

<html lang="ja">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?=$building_name?></title>
<meta name="viewport" content="width=device-width,initial-scale=1">
<link rel="stylesheet" href="building_map_1.css">
</head>
<body>
<div id="wrapper">
  <input type="checkbox" id="navTgl">
 
  <label for="navTgl" class="open">≡</label>
  <label for="navTgl" class="close"></label>
  <nav class="menu">
    <h2>部屋一覧</h2>
    <ul>
	<?php foreach($room_no_array as $room_no){ ?>
      <li>
		  <table>
			<tr>
				<td>
					<form method="POST" action="<?=$_SERVER["PHP_SELF"]?>">
					<input type="hidden" name="roomnumber_no" value="<?=$room_no?>">
					<input type="submit" id="menuSwitch<?=$room_no?>" class="none">
					<label for="menuSwitch<?=$room_no?>">
						<?=$room_no?>号室<br>
						<?php get_data($building_name, $room_no) ?><br>
						<nobr>ビーコン:<?php echo get_beacon_identifier(get_id($building_name, $room_no)); ?></nobr>
					</label>
					</form>
				</td>
				<td>
					<table>
						<tr>
							<form method="POST" action="roomop.php">
							<td><input type="submit" value="更新" class="menuButton"></td>
							<input type="hidden" name="act" value="upd">
							<input type="hidden" name="room_id" value="<?php echo get_id($building_name, $room_no); ?>">
							</form>
						</tr>
					</table>
				</td>
			</tr>
		  </table>
	  </li>
	<?php } ?>
    </ul>
  </nav>
 
  <div class="contents">
<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   id="svg4222"
   version="1.1"
   inkscape:version="0.91 r13725"
   width="1205"
   height="1417.5"
   viewBox="0 0 1205 1417.5"
   sodipodi:docname="building_map_2.svg">
  <metadata
     id="metadata4228">
    <rdf:RDF>
      <cc:Work
         rdf:about="">
        <dc:format>image/svg+xml</dc:format>
        <dc:type
           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
        <dc:title></dc:title>
      </cc:Work>
    </rdf:RDF>
  </metadata>
  <defs
     id="defs4226" />
  <sodipodi:namedview
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1"
     objecttolerance="10"
     gridtolerance="10"
     guidetolerance="10"
     inkscape:pageopacity="0"
     inkscape:pageshadow="2"
     inkscape:window-width="1063"
     inkscape:window-height="666"
     id="namedview4224"
     showgrid="false"
     inkscape:zoom="0.56915012"
     inkscape:cx="353.40427"
     inkscape:cy="983.94869"
     inkscape:window-x="0"
     inkscape:window-y="0"
     inkscape:window-maximized="0"
     inkscape:current-layer="layer2" />
  <g
     inkscape:groupmode="layer"
     id="layer1"
     inkscape:label="Layer 1"
     style="display:inline"
     sodipodi:insensitive="true">
    <image
       sodipodi:absref="/Applications/MAMP/htdocs/PHP/roomop/building_map_2.jpg"
       xlink:href="building_map_2.jpg"
       y="0"
       x="0"
       id="image4230"
       preserveAspectRatio="none"
       height="1417.5"
       width="1205" />
  </g>
  <g
     inkscape:groupmode="layer"
     id="layer2"
     inkscape:label="Layer 2"
     style="display:inline"><!-- なにこれ -->
	<!-- ビーコン操作画面へ遷移するためのスクリプト -->
	<script><![CDATA[
	function beacon_remove(room_id){
		window.open('', 'remove', 'width=800,height=600,scrollbars=yes');
		var form = document.createElement('form');
		document.body.appendChild(form);
		var input = document.createElement('input');
		input.setAttribute('type', 'hidden');
		input.setAttribute('name', 'room_id');
		input.setAttribute('value', room_id);
		form.appendChild(input);
		form.setAttribute('action', 'map_beacon_remove.php');
		form.setAttribute('target', 'remove');
		form.setAttribute('method', 'post');
		form.submit();
	}
	
	function beacon_append(room_id){
		window.open('', 'append', 'width=800,height=600,scrollbars=yes');
		var form = document.createElement('form');
		document.body.appendChild(form);
		var input = document.createElement('input');
		input.setAttribute('type', 'hidden');
		input.setAttribute('name', 'room_id');
		input.setAttribute('value', room_id);
		form.appendChild(input);
		form.setAttribute('action', 'map_beacon_append.php');
		form.setAttribute('target', 'append');
		form.setAttribute('method', 'post');
		form.submit();
	}
	
	function scroll(roomnumber_no, point){
		if(roomnumber_no == <?=$strong_roomnumber_no?>){
			window.scrollTo(0, point);
		}
	}
	]]></script>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       <?php 
       //左上の座標
       $ulx = 89.246634;
       $uly = 103.14048;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 113.58663;
       //右下の座標
       $drx = $dlx + 289.037396;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 112.57246;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4235"
       inkscape:connector-curvature="0">
       <title>情報学科</title>
    </path>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       <?php 
       //部屋番号
       $roomnumber_no = "400";
       //左上の座標
       $ulx = 138.94078;
       $uly = 288.73291;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 65.92081;
       //右下の座標
       $drx = $dlx + 144.01162;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 66.93497;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4237"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
    </path>
    <text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "406";
       //左上の座標
       $ulx = 284.98073;
       $uly = 288.73291;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 65.92081;
       //右下の座標
       $drx = $dlx + 158.20994;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 66.93497;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4239"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="m 441.963,287.22056 0,45.8332 129.31509,0 0,-45.01475 -129.31509,0 z"
       id="path4241"
       inkscape:connector-curvature="0">
	</path>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="m 570.45964,288.85746 0,43.37785 46.65165,0 0,-43.37785 z"
       id="path4243"
       inkscape:connector-curvature="0">
	</path>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "403";
       //左上の座標
       $ulx = 795.53339;
       $uly = 287.22056;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 67.1129;
       //右下の座標
       $drx = $dlx + 317.55861;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 66.29445;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4245"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "407";
       //左上の座標
       $ulx = 88.392599;
       $uly = 387.88991;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 98.214;
       //右下の座標
       $drx = $dlx + 114.583001;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.03245;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4247"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "408";
       //左上の座標
       $ulx = 202.15715;
       $uly = 387.07146;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.8509;
       //右下の座標
       $drx = $dlx + 112.9461;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.8509;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4249"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "409";
       //左上の座標
       $ulx = 315.10325;
       $uly = 387.07146;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.8509;
       //右下の座標
       $drx = $dlx + 127.6782;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.03245;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4251"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "401";
       //左上の座標
       $ulx = 441.14455;
       $uly = 387.88991;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.03245;
       //右下の座標
       $drx = $dlx + 130.13354;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.03245;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4253"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "402";
       //左上の座標
       $ulx = 570.45964;
       $uly = 387.07146;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.8509;
       //右下の座標
       $drx = $dlx + 225.8922;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.8509;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4255"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "404";
       //左上の座標
       $ulx = 795.53339;
       $uly = 387.88991;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.03245;
       //右下の座標
       $drx = $dlx + 127.6782;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.03245;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4257"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "405";
       //左上の座標
       $ulx = 922.39314;
       $uly = 387.88991;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.03245;
       //右下の座標
       $drx = $dlx + 190.69886;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.8509;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4259"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "310";
       //左上の座標
       $ulx = 138.31805;
       $uly = 594.13931;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 66.29445;
       //右下の座標
       $drx = $dlx + 178.4221;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 65.476;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4261"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "302";
       //左上の座標
       $ulx = 315.9217;
       $uly = 594.13931;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 66.29445;
       //右下の座標
       $drx = $dlx + 126.85975;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 65.476;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4263"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="m 442.78145,594.95776 0,45.8332 128.49664,0 0,-45.8332 z"
       id="path4265"
       inkscape:connector-curvature="0">
	</path>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="m 570.45964,594.95776 0,44.1963 48.28855,0 0,-45.01475 -48.28855,0 z"
       id="path4267"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "305";
       //左上の座標
       $ulx = 795.53339;
       $uly = 593.32086;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 67.93135;
       //右下の座標
       $drx = $dlx + 127.6782;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 67.1129;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4269"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "311";
       //左上の座標
       $ulx = 922.39314;
       $uly = 594.13931;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 66.29445;
       //右下の座標
       $drx = $dlx + 191.51726;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 65.476;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4271"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "300";
       //左上の座標
       $ulx = 88.392599;
       $uly = 693.99021;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.03245;
       //右下の座標
       $drx = $dlx + 227.529101;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.03245;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4273"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "301";
       //左上の座標
       $ulx = 315.10325;
       $uly = 693.17176;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.8509;
       //右下の座標
       $drx = $dlx + 127.6782;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 100.66935;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4275"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "303";
       //左上の座標
       $ulx = 441.963;
       $uly = 693.17176;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 101.4878;
       //右下の座標
       $drx = $dlx + 128.49664;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 100.66935;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4277"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "304";
       //左上の座標
       $ulx = 570.45964;
       $uly = 693.99021;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.03245;
       //右下の座標
       $drx = $dlx + 225.8922;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.03245;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4279"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "307";
       //左上の座標
       $ulx = 795.53339;
       $uly = 693.17176;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.8509;
       //右下の座標
       $drx = $dlx + 127.6782;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.8509;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4281"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "306";
       //左上の座標
       $ulx = 922.39314;
       $uly = 692.35331;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 101.4878;
       //右下の座標
       $drx = $dlx + 190.69886;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 100.66935;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4283"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "212";
       //左上の座標
       $ulx = 139.1365;
       $uly = 901.05805;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 67.93135;
       //右下の座標
       $drx = $dlx + 176.7852;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 67.93135;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4323"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "202";
       //左上の座標
       $ulx = 315.9217;
       $uly = 900.2396;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 67.93135;
       //右下の座標
       $drx = $dlx + 126.85975;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 67.93135;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4325"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="m 442.78145,900.2396 0,45.8332 128.49664,0 0,-45.01475 -128.49664,0 z"
       id="path4327"
       inkscape:connector-curvature="0">
	</path>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "207";
       //左上の座標
       $ulx = 796.35184;
       $uly = 901.8765;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 66.29445;
       //右下の座標
       $drx = $dlx + 126.0413;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 67.1129;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4329"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "213";
       //左上の座標
       $ulx = 921.57469;
       $uly = 900.2396;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 67.93135;
       //右下の座標
       $drx = $dlx + 192.33571;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 67.1129;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4331"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "200";
       //左上の座標
       $ulx = 88.392599;
       $uly = 1000.909;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.0324;
       //右下の座標
       $drx = $dlx + 228.347551;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 98.214;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4333"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "201";
       //左上の座標
       $ulx = 315.9217;
       $uly = 1001.7274;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.8509;
       //右下の座標
       $drx = $dlx + 126.85975;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.8509;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4335"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "203";
       //左上の座標
       $ulx = 442.78145;
       $uly = 1001.7274;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 98.214;
       //右下の座標
       $drx = $dlx + 128.49664;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.0324;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4337"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "204";
       //左上の座標
       $ulx = 570.45964;
       $uly = 1001.7274;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.0325;
       //右下の座標
       $drx = $dlx + 99.8509;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.0325;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4339"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "205";
       //左上の座標
       $ulx = 670.31054;
       $uly = 1000.909;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.0324;
       //右下の座標
       $drx = $dlx + 125.22285;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 98.214;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4343"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "206";
       //左上の座標
       $ulx = 796.35184;
       $uly = 1001.7274;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.8509;
       //右下の座標
       $drx = $dlx + 126.0413;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 99.8509;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4345"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "209";
       //左上の座標
       $ulx = 922.39314;
       $uly = 1001.7274;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 99.0325;
       //右下の座標
       $drx = $dlx + 190.69886;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 98.214;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4347"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "109";
       //左上の座標
       $ulx = 88.392599;
       $uly = 1212.0691;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 70.3867;
       //右下の座標
       $drx = $dlx + 228.347551;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 69.5683;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4349"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "101";
       //左上の座標
       $ulx = 316.74015;
       $uly = 1212.0691;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 71.2051;
       //右下の座標
       $drx = $dlx + 125.22285;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 71.2051;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4351"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
       d="m 443.5999,1211.2506 0,49.107 128.49664,0 0,-49.107 z"
       id="path4353"
       inkscape:connector-curvature="0">
	</path>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "105";
       //左上の座標
       $ulx = 795.53339;
       $uly = 1212.0691;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 72.0236;
       //右下の座標
       $drx = $dlx + 126.85975;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 72.0236;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4355"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "106";
       //左上の座標
       $ulx = 922.39314;
       $uly = 1213.706;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 69.5682;
       //右下の座標
       $drx = $dlx + 191.51726;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 70.3867;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       d="m 922.39314,1213.706 0,69.5682 191.51726,0 0,-70.3867 -191.51726,0 z"
       id="path4357"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "100";
       //左上の座標
       $ulx = 87.574149;
       $uly = 1316.8307;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 98.214;
       //右下の座標
       $drx = $dlx + 355.207301;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 98.214;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4359"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "102";
       //左上の座標
       $ulx = 441.963;
       $uly = 1316.0122;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 96.5771;
       //右下の座標
       $drx = $dlx + 128.49664;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 96.5771;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4361"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "103";
       //左上の座標
       $ulx = 571.27809;
       $uly = 1315.1938;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 98.214;
       //右下の座標
       $drx = $dlx + 125.22285;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 97.3956;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4365"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "104";
       //左上の座標
       $ulx = 796.35184;
       $uly = 1316.8307;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 96.5771;
       //右下の座標
       $drx = $dlx + 126.85975;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 98.214;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4367"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
    <path
       style="fill:none;fill-rule:evenodd;stroke:#000000;stroke-width:1px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
	   <?php 
       //部屋番号
       $roomnumber_no = "108";
       //左上の座標
       $ulx = 921.57469;
       $uly = 1316.8307;
       //左下の座標
       $dlx = $ulx + 0.0;
       $dly = $uly + 96.5771;
       //右下の座標
       $drx = $dlx + 191.51731;
       $dry = $dly + 0.0;
       //右上の座標
       $urx = $drx + 0.0;
       $ury = $dry - 96.5771;
       //中心座標
       $sx = ($ulx + $urx) / 2;
       $sy = ($uly + $dly + $font_size) / 2;
       ?>
       d="M <?=$ulx?>,<?=$uly?> <?=$dlx?>,<?=$dly?> <?=$drx?>,<?=$dry?> <?=$urx?>,<?=$ury?> Z"
       id="path4369"
       inkscape:connector-curvature="0">
       <title><?=$roomnumber_no?></title>
	</path>
	<text x="<?=$sx?>" y="<?=$sy?>" font-size="<?=$font_size?>" text-anchor="middle">
            <?php get_data($building_name, $roomnumber_no) ?>
    </text>
  </g>
</svg>

<?php 
function get_data($building_name, $roomnumber_no) {
	global $conn;
	global $strong_roomnumber_no;
	
	//棟IDを取得
	$sql = "SELECT * FROM building ";
	$sql .= " WHERE (building.building_name = '".cnv_sqlstr($building_name)."')";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$building_id = $row["building_id"];
	
	//部屋番号IDを取得
	$sql = "SELECT * FROM roomnumber ";
	$sql .= " WHERE (roomnumber.roomnumber_no =".cnv_sqlstr($roomnumber_no).")";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$roomnumber_id = $row["roomnumber_id"];
	
	//登録されたroomdataのroom_nameを調べる
	$sql = "SELECT * FROM roomdata ";
	$sql .= " WHERE (roomdata.roomnumber_id =".cnv_sqlstr($roomnumber_id).")";
	$sql .= " AND (roomdata.building_id =".cnv_sqlstr($building_id).")";
	$res = db_query($sql, $conn);
	$row = mysqli_fetch_array($res, MYSQL_ASSOC);
	$room_name = $row["room_name"];
	
	if($roomnumber_no === $strong_roomnumber_no){
		$room_name = "<tspan font-weight=\"bolder\" fill=\"red\">" . $room_name . "</tspan>";
	}
	
	echo $room_name;
}

function db_conn() {
//mysqliに変更
$conn = mysqli_connect(DBSV, DBUSER, DBPASS, DBNAME) or die("接続エラー");
return $conn;
}

function cnv_sqlstr($string) {
	$det_enc = mb_detect_encoding($string,"UTF-8");
	if ($det_enc and $det_enc != ENCDB) {
		$string = mb_convert_encoding($string, ENCDB, $det_enc);
	}
	$string = addslashes($string);
	return $string;
}

function db_query($sql, $conn) {
$res = mysqli_query($conn, $sql);
return $res;
}
 ?>
